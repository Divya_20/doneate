package com.example.student.doneate.common;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.student.doneate.ngo.NGOSignUp;
import com.example.student.doneate.R;
import com.example.student.doneate.restaurant.ResSignUp;

public class ChooseTypeActivity extends AppCompatActivity {

    RadioGroup radioType;
    RadioButton selectedType;
    ImageView back_to_main;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_type);

        back_to_main = findViewById(R.id.ch_back);
        back_to_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent itBackToMain = new Intent(ChooseTypeActivity.this,MainActivity.class);
                startActivity(itBackToMain);
            }
        });
        radioType = findViewById(R.id.radioType);
        radioType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                selectedType = findViewById(checkedId);
                if(selectedType.getText().equals("Restaurant")){

                    Intent itSignUp = new Intent(ChooseTypeActivity.this, ResSignUp.class);
                    startActivity(itSignUp);
                }
                else{
                    Intent itNGO = new Intent(ChooseTypeActivity.this, NGOSignUp.class);
                    startActivity(itNGO);
                }
            }
        });

    }
}
