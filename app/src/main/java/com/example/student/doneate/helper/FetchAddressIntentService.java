package com.example.student.doneate.helper;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;
import java.util.Locale;

public class FetchAddressIntentService extends IntentService {
    private static final String IDENTIFIER = "FetchAddressIntentService";
    private ResultReceiver addressResultReceiver;

    public FetchAddressIntentService(){
        super(IDENTIFIER);
    }
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String msg = "";
        addressResultReceiver = intent.getParcelableExtra("add_receiver");

        if(addressResultReceiver == null){
            Log.e("FetchAddress", "No receiver, not processing the request further");
            return;
        }
        Location location = intent.getParcelableExtra("add_location");
        if(location == null){
            msg = "No location, can't go further without location";
            sendResultsToReceiver(0, msg);
            return;
        }
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;

        try{
            Double latitude = location.getLatitude();
            Double longitude = location.getLongitude();
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
        }catch (Exception IOException){
            IOException.printStackTrace();
            Log.d("Error Message:",IOException.getMessage());
            Log.e("","Error in getting address for the location");
        }

        if(addresses == null || addresses.size() == 0){
            msg = "No address found!";
            sendResultsToReceiver(1, msg);
        }else {
            Address address = addresses.get(0);
            StringBuffer addressDetails = new StringBuffer();

            //0
            addressDetails.append(String.valueOf(location.getLatitude()));
            addressDetails.append("\n");
            //1
            addressDetails.append(String.valueOf(location.getLongitude()));
            addressDetails.append("\n");
            //2
            for(int i = 0; i <= address.getMaxAddressLineIndex(); i++){
                addressDetails.append(address.getAddressLine(i));
                addressDetails.append(",");
            }
            addressDetails.append("\n");
//            //3
//            if(address.getThoroughfare() != null){
//                addressDetails.append("ThoroughFare " + address.getThoroughfare());
//            }else{
//                addressDetails.append("");
//            }
//            addressDetails.append("\n");
//            //4
//            if(address.getSubLocality() != null){
//                addressDetails.append("Locality " + address.getLocality());
//            }else{
//                addressDetails.append("");
//            }
//            addressDetails.append("\n");
//            //5
//            if(address.getSubLocality() != null){
//                addressDetails.append("Sublocality " + address.getSubLocality());
//            }else{
//                addressDetails.append("");
//            }
//            addressDetails.append("\n");
//            //6
//            if(address.getFeatureName() != null){
//                addressDetails.append("Feature " + address.getFeatureName());
//            }else{
//                addressDetails.append("");
//            }
//            3 addressDetails.append("City:");
            addressDetails.append(address.getLocality());
            addressDetails.append("\n");

//            4 addressDetails.append("State:");
            addressDetails.append(address.getAdminArea());
            addressDetails.append("\n");

//           5 addressDetails.append("Postal Code: ");
            addressDetails.append(address.getPostalCode());
            addressDetails.append("\n");

//            addressDetails.append(address.getFeatureName());
//            addressDetails.append("\n");
//
//            addressDetails.append(address.getAddressLine(0));
//            addressDetails.append("\n");
//
//            addressDetails.append("Locality: ");
//
//
//            addressDetails.append("City: ");
//            addressDetails.append(address.getSubAdminArea());
//            addressDetails.append("\n");
//
//            addressDetails.append("State: ");
//
//
//            addressDetails.append("Country: ");
//            addressDetails.append(address.getCountryName());
//            addressDetails.append("\n");
//
//
//            addressDetails.append("\n");

            sendResultsToReceiver(2, addressDetails.toString());
        }
    }

    private void sendResultsToReceiver(int resultCode, String message) {
        Bundle bundle = new Bundle();
        bundle.putString("address_result", message);
        addressResultReceiver.send(resultCode, bundle);
    }
}
