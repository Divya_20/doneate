package com.example.student.doneate.ngo;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.student.doneate.R;
import com.example.student.doneate.helper.JSONParser;
import com.example.student.doneate.helper.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import customfonts.MyEditText;

public class ShowAllQC extends Fragment {

    View rootView;
    private static String url_qc_details = "http://10.112.68.50/doneate/get_qc_details.php";
    private static String serverUrl = "https://doneate.000webhostapp.com/get_qc_details.php";

    private static String url_updt_qc_details = "http://10.112.68.50/doneate/update_qc_details.php";
    private static String serverUrlUpdtQc = "https://doneate.000webhostapp.com/update_qc_details.php";

    JSONParser jsonParser = new JSONParser();

    TextView tvname, tvphn, tvmail, tvqual, tvGender;
    String name, phn, mail, qual, gender;
    String newname, newPhn, newmail, newGender, newQual;
    EditText edname, edphn, edmail, edqual, edGender;
    FloatingActionButton fabEdit;
    Button btnSave;
    String userId;
    String qcId;

    public ShowAllQC() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_show_all_qc, container, false);
        tvname = rootView.findViewById(R.id.qcname);
        tvphn = rootView.findViewById(R.id.tvqcphn);
        tvmail = rootView.findViewById(R.id.tvqcmail);
        tvGender = rootView.findViewById(R.id.tvgender);
        tvqual = rootView.findViewById(R.id.tvqcqual);

        edname = rootView.findViewById(R.id.edqcname);
        edphn = rootView.findViewById(R.id.edqcphn);
        edmail = rootView.findViewById(R.id.edqcmail);
        edGender = rootView.findViewById(R.id.edqcgender);
        edqual = rootView.findViewById(R.id.edqcqual);
        btnSave = rootView.findViewById(R.id.btnDoneEdit);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newname = edname.getText().toString().trim();
                newmail = edmail.getText().toString().trim();
                newPhn = edphn.getText().toString().trim();
                newGender = edGender.getText().toString().trim();
                newQual = edqual.getText().toString().trim();
                if(!newname.equals("") || newname != null ||
                    !newmail.equals("") || newmail != null ||
                    !newPhn.equals("") || newPhn != null ||
                    !newGender.equals("") || newGender != null ||
                    !newQual.equals("") || newQual != null
                ) {
                    new UpdateQcData().execute();
                    changeView(true);
                }else{
                    Toast.makeText(getContext(), "One or more fields empty!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        fabEdit = rootView.findViewById(R.id.fab);
        fabEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setVisibility(View.GONE);
                changeView(false);
            }
        });

        new GetData().execute();
        return rootView;
    }

    public void changeView(boolean changed){
        if(!changed) {
            tvname.setVisibility(View.GONE);
            tvphn.setVisibility(View.GONE);
            tvmail.setVisibility(View.GONE);
            tvGender.setVisibility(View.GONE);
            tvqual.setVisibility(View.GONE);

            edname.setVisibility(View.VISIBLE);
            edmail.setVisibility(View.VISIBLE);
            edphn.setVisibility(View.VISIBLE);
            edGender.setVisibility(View.VISIBLE);
            edqual.setVisibility(View.VISIBLE);
            btnSave.setVisibility(View.VISIBLE);
        }else{
            tvname.setVisibility(View.VISIBLE);
            tvphn.setVisibility(View.VISIBLE);
            tvmail.setVisibility(View.VISIBLE);
            tvGender.setVisibility(View.VISIBLE);
            tvqual.setVisibility(View.VISIBLE);

            edname.setVisibility(View.GONE);
            edmail.setVisibility(View.GONE);
            edphn.setVisibility(View.GONE);
            edGender.setVisibility(View.GONE);
            edqual.setVisibility(View.GONE);
            btnSave.setVisibility(View.GONE);
        }
    }
    
    private class GetData extends AsyncTask<String, String, String> {
        String msg;
        ProgressDialog loading = new ProgressDialog(getContext());
        @Override
        protected void onPreExecute(){
            loading.setMessage("Loading Profile..");
            loading.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try{
                SessionManager userSession = new SessionManager(getContext());
                userId = userSession.getUserId();
                int success;
                List<NameValuePair> args = new ArrayList<NameValuePair>();
                args.add(new BasicNameValuePair("n_id",userId));

                JSONObject json = jsonParser.makeHttpRequest(
                        url_qc_details, "GET", args);
                Log.d("QC Details", json.toString());
                success = json.getInt("success");
                if(success == 1){

                    JSONArray restaurantObj = json.getJSONArray("qc_person");
                    JSONObject restaurant = restaurantObj.getJSONObject(0);
                    name = restaurant.getString("name");
                    mail = restaurant.getString("uname");
                    phn = restaurant.getString("phone");
                    gender = restaurant.getString("gender");
                    qual = restaurant.getString("qualification");
                    qcId = restaurant.getString("qc_id");
                }else{
                    msg = "No qc found!";
                }
            }catch(JSONException j){
                msg = j.getMessage().toString();
            }catch (Exception e){
                msg = "Poor internet connection";
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            loading.dismiss();
//            if(s != null){
            tvname.setText(name);
            tvmail.setText(mail);
            tvGender.setText(gender);
            tvphn.setText(phn);
            tvqual.setText(qual);

            edname.setText(name);
            edmail.setText(mail);
            edGender.setText(gender);
            edphn.setText(phn);
            edqual.setText(qual);
//            }
            super.onPostExecute(s);
        }
    }

    private class UpdateQcData extends AsyncTask<String, String, String>{
        ProgressDialog savingChanges;
        String msg = "";
        @Override
        protected void onPreExecute() {
            savingChanges = new ProgressDialog(getContext());
            savingChanges.setMessage("Saving Changes");
            savingChanges.setIndeterminate(false);
            savingChanges.setCancelable(false);
            savingChanges.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("name", newname));
            pairs.add(new BasicNameValuePair("gender", newGender));
            pairs.add(new BasicNameValuePair("phn", newPhn));
            pairs.add(new BasicNameValuePair("mail", newmail));
            pairs.add(new BasicNameValuePair("qual", newQual));
            pairs.add(new BasicNameValuePair("qc_id", qcId));
            JSONObject jsonObject = new JSONParser().makeHttpRequest(url_updt_qc_details,"POST",pairs);
            try{
                int success = jsonObject.getInt("success");
                if(success == 1){
                    msg = "Updated Successfully!";
                }else{
                    msg = "Could not Update";
                }
            }catch (JSONException j){
                j.printStackTrace();
            }catch (Exception e){
                e.printStackTrace();
                msg = "Poor internet connection";
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            savingChanges.dismiss();
            Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
            new GetData().execute();

        }
    }
}
