package com.example.student.doneate.ngo;

public class CurrentRequests {
    private String reqId;
    private String resName;
    private String  qty;
    private String imgRes;
    private String itemName;
    private String reqTime;
    private int reqStatusRes;
    private int reqStatusQc;

    public CurrentRequests() {
    }

    public String getItemName() {
        return itemName;
    }

    public String getReqTime() {
        return reqTime;
    }

    public String getReqId() {
        return reqId;
    }

    public CurrentRequests(String reqId, String qty, String itemName, String reqTime, String imgRes, int reqStatusRes, int reqStatusQc, String resName) {
        this.qty = qty;
        this.imgRes = imgRes;
        this.reqStatusQc = reqStatusQc;
        this.reqStatusRes = reqStatusRes;
        this.resName = resName;
        this.itemName = itemName;
        this.reqTime = reqTime;
        this.reqId = reqId;
    }

    public String getQty() {
        return qty;
    }

    public String getImgRes() {
        return imgRes;
    }

    public int getResApprovalStatus() {
        return reqStatusRes;
    }

    public int getQcApprovalStatus() {
        return reqStatusQc;
    }

    public String getResName() {
        return resName;
    }
}
