package com.example.student.doneate.ngo;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.student.doneate.R;
import com.example.student.doneate.helper.JSONParser;
import com.example.student.doneate.helper.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CurrentRequestsNGO extends Fragment {

    View rootview;
    JSONParser jParser = new JSONParser();
    JSONObject joAllRequests = new JSONObject();
    private static String url_current_req = "http://10.112.68.50/doneate/get_current_req.php";
    private static String serverCurrentReq = "https://doneate.000webhostapp.com/get_current_req.php";

    int noOfReq;
    JSONArray jaCurRequests = null;
    List<CurrentRequests> currentRequests = new ArrayList<CurrentRequests>();;
    CurrentRequestsAdapter adapter;
    String resName, ngoId;
    Double qty;
    static RecyclerView rvCurReq;
    ImageView imgNothing;
    public CurrentRequestsNGO() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_current_requests, container, false);
        rvCurReq = rootview.findViewById(R.id.rvCurrentRequests);
        imgNothing = rootview.findViewById(R.id.imgNothing);
        ngoId = new SessionManager(getContext()).getUserId();
        if(ngoId != null || !ngoId.equals("")){
            new LoadCurrentRequests().execute();
        }
        return rootview;
    }

    private class LoadCurrentRequests extends AsyncTask<String, String, String>{

        ProgressDialog loading;
        @Override
        protected void onPreExecute() {
            loading = new ProgressDialog(getContext());
            loading.setMessage("Loading...");
            loading.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String msg = "";
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("n_id",ngoId));
            JSONObject jsonObject = jParser.makeHttpRequest(url_current_req, "GET", pairs);
            Log.d("Current Requests:",jsonObject.toString());

            try {
                int success = jsonObject.getInt("success");
                if(success == 1){
                    jaCurRequests = jsonObject.getJSONArray("curr_req");
                    if(jaCurRequests.length() > 0){
                        noOfReq = jaCurRequests.length();
                        for (int i = 0; i < jaCurRequests.length(); i++){
                            JSONObject object = jaCurRequests.getJSONObject(i);
                            int res_app = 0, qc_app = 0;
                            String req_id = object.getString("req_id");
                            String qty = object.getString("req_qty");
                            String item_name = object.getString("item_name");
                            String resName = object.getString("res_name");
                            String resImgUrl = object.getString("r_id");
                            String req_time = object.getString("req_time");
                            int req_status = object.getInt("req_status");
                            if(req_status == 0){
                                res_app = 0; qc_app = 0;
                            }else if(req_status == 1){
                                res_app = 1; qc_app = 0;
                            }else if(req_status == 2){
                                res_app = 1; qc_app = 1;
                            }
                            currentRequests.add(new CurrentRequests(req_id,qty,item_name, req_time, resImgUrl, res_app, qc_app, resName));
                        }
                    }else{
                        noOfReq = 0;
                    }
                }else{
                    msg = jsonObject.getString("error");
                }
            }catch (JSONException j){
                j.printStackTrace();
                msg = j.getMessage();
            }catch (Exception e){
                e.printStackTrace();
                msg = e.getMessage();
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            loading.dismiss();
            if(currentRequests.size() > 0) {
                adapter = new CurrentRequestsAdapter(getContext(), currentRequests);
                rvCurReq.setLayoutManager(new GridLayoutManager(getContext(), 2));
                rvCurReq.setAdapter(adapter);
            }else{
                imgNothing.setVisibility(View.VISIBLE);
//                Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
            }
        }
    }





}
