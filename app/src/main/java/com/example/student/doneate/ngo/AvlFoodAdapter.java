package com.example.student.doneate.ngo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.student.doneate.R;
import com.example.student.doneate.ngo.AvailableFood;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class AvlFoodAdapter extends BaseAdapter {
    private Activity activity;
//    private ArrayList<HashMap<String, String>> data;
    private List<Food> data;
    private static LayoutInflater inflater=null;
    Bitmap image,receivedBitmap;
    Date uploadDateTime,prepDateTime;
    HashMap<String, String> foodMap = new HashMap<String, String>();

    public AvlFoodAdapter(Activity a, List<Food> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
////        HashMap<String, String> food = new HashMap<String, String>();
//
//        food = data.get(position);
//            return food.get(AvailableFood.KEY_ID);
        Food food = data.get(position);
        return food.getFoodId();

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public AvlFoodAdapter() {
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        String prepTime="",uploadTime="";
        if(convertView==null)
            vi = inflater.inflate(R.layout.list_row_avl_food, null);
        TextView fid = vi.findViewById(R.id.lv_f_id);
        TextView name = vi.findViewById(R.id.item_name);
        TextView qty = vi.findViewById(R.id.item_qty);
        TextView upload_time = vi.findViewById(R.id.item_upload);
        TextView prep_time = vi.findViewById(R.id.item_prepared);
        TextView resName = vi.findViewById(R.id.resName);
        ImageView type = vi.findViewById(R.id.type);
        ImageView thumb_image = vi.findViewById(R.id.res_image);
        RatingBar ratingBar = vi.findViewById(R.id.pop_ratingbar);
//        HashMap<String, String> food = new HashMap<String, String>();
        Food food = data.get(position);
//        try {
//            SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
////            uploadTime = food.get(AvailableFood.KEY_UPLOAD_TIME);
////            prepTime = food.get(AvailableFood.KEY_PREP_TIME);
////            uploadDateTime = dateParser.parse(uploadTime);
////            prepDateTime = dateParser.parse(prepTime);
//            uploadTime = food.getUploadTime();
//            prepTime = food.getPrepTime();
//            uploadDateTime  = dateParser.parse(uploadTime);
//            prepDateTime = dateParser.parse(prepTime);
//            uploadTime = dateFormatter.format(uploadDateTime).toString();
//            prepTime = dateFormatter.format(prepDateTime).toString();
////            upload_time.setText(dateFormatter.format(uploadDateTime));
////            prep_time.setText(dateFormatter.format(prepDateTime));
////            System.out.println(dateFormatter.format(date));
////            uploadTime = sdf2.format(sdf.parse());
////            prepTime = sdf2.format(sdf.parse(food.get(AvailableFood.KEY_PREP_TIME)));
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        fid.setText(food.get(AvailableFood.KEY_ID));
//        name.setText(food.get(AvailableFood.KEY_NAME));
//        qty.setText(food.get(AvailableFood.KEY_QTY) + " kg");
//        upload_time.setText(uploadTime);
//        prep_time.setText(prepTime);
//        resName.setText(food.get(AvailableFood.KEY_RESNAME));
//        if(food.get(AvailableFood.KEY_TYPE).equals("Veg"))
//            type.setImageResource(R.drawable.veg);
//        else if(food.get(AvailableFood.KEY_TYPE).equals("Non-Veg"))
//            type.setImageResource(R.drawable.non_veg);
//        thumb_image.setImageBitmap();
//        String photo_url =  "http://10.112.68.50/doneate/res_images/IMG" + food.get(AvailableFood.KEY_RES_ID) + ".JPG" ;
        fid.setText(food.foodId);
        name.setText(food.itemName);
        qty.setText(food.qty.toString()+ " kg");
        upload_time.setText(food.uploadTime);
        prep_time.setText(food.prepTime.toString());
        resName.setText(food.resName);
        ratingBar.setRating(Float.parseFloat(food.rating));
        if(food.type.equals("Veg")){
            type.setImageResource(R.drawable.veg);
        }else{
            type.setImageResource(R.drawable.non_veg);
        }
        String  photo_url = "http://10.112.68.50/doneate/res_images/IMG" + food.resId + ".JPG" ;
        GetImage getImage = new GetImage(thumb_image,photo_url);
        getImage.execute();
        return vi;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        data.clear();
        if (charText.length() == 0) {
            data.addAll(data);
        } else {
            for (Food food : data) {
                if (food.getQty() >= Double.parseDouble(charText)) {
                    data.add(food);
                }
            }
        }
        notifyDataSetChanged();
    }

    private class GetImage extends AsyncTask<Void, Void, Bitmap> {
        ImageView imageView;
        String name;
        public GetImage(ImageView imageView, String name){
            this.imageView = imageView;
            this.name = name;
        }
        @Override
        protected Bitmap doInBackground(Void... voids) {

            try{

                URLConnection connection = new URL(name).openConnection();
                connection.setConnectTimeout(1000 * 30);
                connection.setReadTimeout(1000 * 30);
                receivedBitmap = BitmapFactory.decodeStream((InputStream)connection.getContent(),null,null);
                receivedBitmap = Bitmap.createScaledBitmap(receivedBitmap,100,100,false);
                return receivedBitmap;
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Bitmap bitmap){
            super.onPostExecute(bitmap);
            if(bitmap != null){
                imageView.setImageBitmap(receivedBitmap);
            }
        }
    }

    public void updateData(){

        AvlFoodAdapter.this.notifyDataSetChanged();
    }

}
