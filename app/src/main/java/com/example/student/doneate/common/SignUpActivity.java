package com.example.student.doneate.common;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.student.doneate.R;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import customfonts.MyEditText;

public class SignUpActivity extends AppCompatActivity {

    ImageView go_back;
    String ServerURL = "jdbc:mysql://10.112.68.50/doneate";
    private static final String USER = "doneate";
    private static final String PASS = "doneate";

    MyEditText res_name, res_email,res_pwd, res_cnf_pwd, res_phn_no, res_addLine1, res_addLine2;

    Button btn_sign_up;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        res_name = findViewById(R.id.res_name);
        res_email = findViewById(R.id.email);
        res_phn_no = findViewById(R.id.phone_no);
        //res_alt_phn_no = findViewById(R.id.alt_phn_no);
        res_addLine1 = findViewById(R.id.add_line1);

        res_addLine2 = findViewById(R.id.add_line2);
        res_pwd = findViewById(R.id.pwd);
        res_cnf_pwd = findViewById(R.id.cnf_pwd);

        btn_sign_up = findViewById(R.id.btn_sign_up);

        btn_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Send objSendData = new Send();
//                objSendData.execute();

//                InsertData(name, email, phn_no, locality, landmark, city, pwd);
            }

        });
        go_back = findViewById(R.id.sup_back);
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent itBackToMain = new Intent(SignUpActivity.this, MainActivity.class);
                startActivity(itBackToMain);
            }
        });

    }
    private class Send extends AsyncTask<String, String, String>{
        String msg = "";
        String name, email, phn_no, pwd, addLine1, addLine2;

        @Override
        protected void onPreExecute(){
            name = res_name.getText().toString();
            email = res_email.getText().toString();
            phn_no = res_phn_no.getText().toString();
            pwd = res_pwd.getText().toString();
            addLine1 = res_addLine1.getText().toString();
            addLine2 = res_addLine2.getText().toString();
//            city = res_city.getText().toString();
        }
        @Override
        protected String doInBackground(String... params){
            try{
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn = DriverManager.getConnection(ServerURL,USER,PASS);
                if(conn == null){
                    msg = "Connection goes null";
                }else {
                    PreparedStatement insQuery = conn.prepareStatement("INSERT INTO tblrestaurant(name, email, pwd, phn_no, locality, landmark) VALUES(?,?,?,?,?,?)");
                    insQuery.setString(1,name);
                    insQuery.setString(2,email);
                    insQuery.setString(3,pwd);
                    insQuery.setString(4,phn_no);
                    insQuery.setString(5,addLine1);
                    insQuery.setString(6,addLine2);
//                    insQuery.setString(7,city);

                    int i = insQuery.executeUpdate();
                    if(i > 0){
                        msg = "Registered Successfully";
                    }
                    else{
                        msg = "Please try again";
                    }
                    conn.close();
                }
            }catch (Exception e){
                msg = "Connection goes wrong";
                msg = e.toString();
                e.printStackTrace();
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String msg){
            Toast.makeText(SignUpActivity.this, msg, Toast.LENGTH_LONG).show();
        }
    }

//    public void InsertData(final String name,final String email, final String phn_no,
//                           final String locality, final String landmark, final String city, final String pwd){
//        Log.d("Check","Hi");
//        class SendPostReqAsyncTask extends AsyncTask<String , Void, String> {
//            String msg;
//            @Override
//            protected String doInBackground(String... params){
//
//                List<NameValuePair> resData = new ArrayList<NameValuePair>();
//                resData.add(new BasicNameValuePair("name", name));
//                resData.add(new BasicNameValuePair("email", email));
//                resData.add(new BasicNameValuePair("phn_no", phn_no));
//                //resData.add(new BasicNameValuePair("alt_phn_no", alt_phn_no));
//                resData.add(new BasicNameValuePair("locality", locality));
//                resData.add(new BasicNameValuePair("landmark", landmark));
//                resData.add(new BasicNameValuePair("city", city));
//                resData.add(new BasicNameValuePair("pwd",pwd));
//                Log.d("Data:",resData.toString());
//                Log.d("Check","Hi");
//                try{
//                    HttpClient httpClient = new DefaultHttpClient();
//                    HttpPost httpPost = new HttpPost(ServerURL);
//                    httpPost.setEntity(new UrlEncodedFormEntity(resData));
//                    HttpResponse httpResponse = httpClient.execute(httpPost);
//                    HttpEntity httpEntity = httpResponse.getEntity();
//
//                    msg = "Registered Successfully";
//                }catch (ClientProtocolException c){
//                    msg = c.toString();
//                    c.printStackTrace();
//                }catch (IOException e){
//                    msg = e.toString();
//                    e.printStackTrace();
//                }
//                return msg;
//
//            }
//            @Override
//            protected void onPostExecute(String result){
//                super.onPostExecute(result);
//                Log.d("Result:", result);
//                Toast.makeText(SignUpActivity.this,"Registered Successfully", Toast.LENGTH_LONG).show();
//            }
//
//        }
//        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
//        sendPostReqAsyncTask.execute(name,email,phn_no,locality,landmark,city,pwd);
//    }
}
