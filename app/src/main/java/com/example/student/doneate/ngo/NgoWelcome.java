package com.example.student.doneate.ngo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.student.doneate.R;
import com.example.student.doneate.helper.SessionManager;

public class NgoWelcome extends Fragment {

    View rootView;
    TextView tvNgoWelcomeName;
    Button btnAddQc, btnSkipForNow;
    String ngoName;
    public NgoWelcome() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_ngo_welcome, container, false);
        Bundle arguments = getArguments();
        if(arguments != null){
            ngoName = arguments.getString("ngoName");
        }
        tvNgoWelcomeName = rootView.findViewById(R.id.tvNgoNameHead);
        tvNgoWelcomeName.setText(ngoName);
        btnAddQc = rootView.findViewById(R.id.btnAddQc);
        btnSkipForNow = rootView.findViewById(R.id.btnSkipForNow);

        btnAddQc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.frames_ngo, new AddQcPerson());
                ((NgoHomeActivity)getActivity()).setActionBarTitle("Add/Edit QC");
                tx.commit();

            }
        });
        btnSkipForNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.frames_ngo, new AvailableFood());
                ((NgoHomeActivity)getActivity()).setActionBarTitle("Available Food");
                tx.commit();
            }
        });
        return rootView;
    }

}
