package com.example.student.doneate.restaurant;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.student.doneate.R;
import com.example.student.doneate.common.SignInActivity;
import com.example.student.doneate.helper.JSONParser;
import com.example.student.doneate.helper.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ResHomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AddFood.GetUploadFoodData {
    DrawerLayout drawer;
    NavigationView navigationView;
    Button btnLogout, btnCancel;
    String foodName, foodType, foodPrepTime, foodQty, foodId, resId;
    String userId, name, username, photo_url;
    private static String url_upload_food = "http://10.112.68.50/doneate/upload_food.php";
    private static String serverUrlUploadFood = "https://doneate.000webhostapp.com/upload_food.php";

    private static String url_res_details = "http://10.112.68.50/doneate/get_res_details.php";
    private static String serverUrlResDetails = "https://doneate.000webhostapp.com/get_res_details.php";

    JSONParser jsonParser = new JSONParser();
    ImageView ivNavRes;
    TextView tvNavResName, tvResUname;
    Bitmap receivedBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_res_home);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer =  findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        View layoutNavHeader = navigationView.getHeaderView(0);

        ivNavRes = layoutNavHeader.findViewById(R.id.ivNavRes);
        tvResUname = layoutNavHeader.findViewById(R.id.tvNavResUname);
        tvNavResName  = layoutNavHeader.findViewById(R.id.tvNavResName);


        navigationView.setNavigationItemSelectedListener(this);
        SessionManager checkUserSessionManager = new SessionManager(getApplicationContext());
//        String username = checkUserSessionManager.getUsername();
        userId = checkUserSessionManager.getUserId();
//        String userType = checkUserSessionManager.getUserType();
        if(userId != null && (!userId.equals(""))) {
            navigationView.getMenu().findItem(R.id.nav_res_login).setVisible(false);
            new GetData().execute();
        }else{
            navigationView.getMenu().findItem(R.id.nav_logout).setVisible(false);
        }
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.res_home, menu);
        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.frames_res, new FoodRequests());
        tx.commit();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        selectItemDrawer(item);
        int id = item.getItemId();

        if (id == R.id.nav_add_food) {

        } else if (id == R.id.nav_past) {

        } else if (id == R.id.nav_profile) {

        } else if (id == R.id.nav_logout) {

        }

//        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void selectItemDrawer(MenuItem menuItem){
        Fragment fragment = null;
        Class FragmentClass;

        switch (menuItem.getItemId()){
            case R.id.nav_add_food:
                FragmentClass = AddFood.class;
                break;
            case R.id.nav_past:
                FragmentClass = FragmentPastDonations.class ;
                break;
            case R.id.nav_req_food:
                FragmentClass  = FoodRequests.class;
                break;
            case R.id.nav_profile:
                FragmentClass = ResProfile.class;
                break;
            case R.id.nav_logout:
                FragmentClass = ResProfile.class;
                showPopup();
                break;
            case R.id.nav_res_login:
                Intent itSignIn = new Intent(ResHomeActivity.this, SignInActivity.class);
                startActivity(itSignIn);

            default:
                FragmentClass = FoodRequests.class ;
        }
        try{
            fragment = (Fragment)FragmentClass.newInstance();
        }catch (Exception e){
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frames_res, fragment).commit();
        menuItem.setChecked(true);
        setTitle(menuItem.getTitle());
        drawer.closeDrawers();
    }

    private void showPopup() {
        ViewGroup viewGroup = findViewById(R.id.frames_res);
        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog, viewGroup, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);

        final AlertDialog alert = builder.create();
        alert.show();
        btnLogout = dialogView.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionManager clearSessionManager = new SessionManager(getApplicationContext());
                clearSessionManager.clearAll();

                Intent loginIntent = new Intent(ResHomeActivity.this, SignInActivity.class);
                loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loginIntent);
                finish();
            }
        });
        btnCancel = dialogView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
    }

    @Override
    public void onUploadFoodData(String qty, String type, String name, String prep_time ){
        foodName = name; foodType = type; foodQty = qty; foodPrepTime = prep_time;
    }

    public void uploadFood(){
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        SessionManager getRestaurant = new SessionManager(getApplicationContext());
        resId = getRestaurant.getUserId();
        foodId = "F" + resId.substring(1,5) + foodName.substring(1,4) + ts;

        Send sendFoodUpload = new Send();
        sendFoodUpload.execute();
    }

    private class Send extends AsyncTask<String, String, String >{
        String msg = "";
        ProgressDialog uploading;

        @Override
        protected void onPreExecute(){
            uploading = new ProgressDialog(ResHomeActivity.this);
            uploading.setMessage("Uploading...");
            uploading.setIndeterminate(false);
            uploading.setCancelable(false);
            uploading.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> foodData = new ArrayList<NameValuePair>();
            foodData.add(new BasicNameValuePair("f_id",foodId));
            foodData.add(new BasicNameValuePair("item_name",foodName));
            foodData.add(new BasicNameValuePair("type",foodType));
            foodData.add(new BasicNameValuePair("prep_time",foodPrepTime));
            foodData.add(new BasicNameValuePair("qty",foodQty));
            foodData.add(new BasicNameValuePair("r_id",resId));

            JSONObject json = jsonParser.makeHttpRequest(url_upload_food,"POST", foodData);
//            Log.d("Create Response", json.toString());
            try {
                int success = json.getInt("success");

                if (success == 1) {
                    msg = "Uploaded Successfully";

                } else {
                    msg = "Couldn't Upload Food";
                }
            } catch (JSONException e) {
                msg = e.getMessage().toString();
                e.printStackTrace();
            }

            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            uploading.dismiss();
            Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
        }
    }

    private class GetData extends AsyncTask<String, String, String>{
        String msg;

        @Override
        protected void onPreExecute(){

        }

        @Override
        protected String doInBackground(String... strings) {
            try{

                int success;
                List<NameValuePair> args = new ArrayList<NameValuePair>();
                args.add(new BasicNameValuePair("r_id",userId));

                JSONObject json = jsonParser.makeHttpRequest(
                        url_res_details, "GET", args);
                Log.d("Restaurant Details", json.toString());
                success = json.getInt("success");
                if(success == 1){

                    JSONArray restaurantObj = json.getJSONArray("restaurant");
                    JSONObject restaurant = restaurantObj.getJSONObject(0);
                    name = restaurant.getString("name");
                    username = restaurant.getString("uname");
                    photo_url = restaurant.getString("photo");
                }else{
                    msg = "No restaurant found!";
                }
            }catch(JSONException j){
                msg = j.getMessage().toString();
            }catch (Exception e){
                msg = "Poor internet connection";
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            tvNavResName.setText(name);
            tvResUname.setText(username);
            new GetImage("res_images/IMG" + userId + ".JPG").execute();
        }
    }

    private class GetImage extends AsyncTask<Void, Void, Bitmap>{
        String name;
        public GetImage(String name){
            this.name = name;
        }
        @Override
        protected Bitmap doInBackground(Void... voids) {
            String serverGetPhoto = "https://doneate.000webhostapp.com/res_images/IMG"+ resId + ".JPG";
            photo_url = "http://10.112.68.50/doneate/" + name;
            try{
                URLConnection connection = new URL(photo_url).openConnection();
                connection.setConnectTimeout(1000 * 30);
                connection.setReadTimeout(1000 * 30);
                receivedBitmap = BitmapFactory.decodeStream((InputStream)connection.getContent(),null,null);
                return receivedBitmap;
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Bitmap bitmap){
            super.onPostExecute(bitmap);
            if(bitmap != null){
                ivNavRes.setImageBitmap(receivedBitmap);
            }
        }
    }
}
