package com.example.student.doneate.qc.ui.home;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.annotation.Nullable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.example.student.doneate.R;
import com.example.student.doneate.helper.JSONParser;
import com.example.student.doneate.helper.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FragmentHomeQc extends Fragment {

    View rootView;
    static final String KEY_NAME = "item_name";
    static final String KEY_REQ_QTY = "qty";
    static final String KEY_ID = "req_id";
    static final String KEY_TYPE = "type";
    static final String KEY_RES_NAME = "name";
    static final String KEY_RES_ID ="n_id";
    static final String KEY_RES_ADD = "res_add";
    static final String KEY_FOOD_ID = "f_id";
    static final String KEY_PREP_TIME = "prep_time";
    static final String KEY_UPLOAD_TIME = "upload_time";
    static final String KEY_REQ_TIME = "req_time";
    static final String KEY_REQ_STATUS = "req_status";

    JSONObject joAllRequests = new JSONObject();
    JSONParser jParser = new JSONParser();
    JSONObject temp = new JSONObject();
    ArrayList<HashMap<String, String>> arrListReqFood;
    ReqToConfirmAdapter adapter;
    JSONArray jaRequestedFood = null;
    SwipeMenuListView lvReqFood;
    FloatingActionButton btnConfirmAll, btnConfirMultiple;
    ImageView imgNothingToShow;
    String qcId;
    private static String url_req_to_confirm = "http://10.112.68.50/doneate/req_to_confirm_qc.php";
    private static String serverReqToConfirm = "https://doneate.000webhostapp.com/req_to_confirm_qc.php";

    private static String url_updt_food = "http://10.112.68.50/doneate/update_food_req.php";
    private static String serverUpdtFood = "https://doneate.000webhostapp.com/update_food_req.php";
    int noOfReq;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_qc_home,container, false);
        imgNothingToShow = rootView.findViewById(R.id.imgNothing);
        lvReqFood = rootView.findViewById(R.id.list_req_to_confirm);
        arrListReqFood = new ArrayList<HashMap<String, String>>();
        SessionManager sessionManager = new SessionManager(getContext());
        qcId = sessionManager.getUserId();
        if(qcId != null || !qcId.equals("")){
            new LoadReqToConfirm().execute();
        }
        btnConfirmAll = rootView.findViewById(R.id.btnConfirmAll);
        btnConfirMultiple = rootView.findViewById(R.id.btnConfirmMultiple);
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getContext());
                deleteItem.setBackground(R.drawable.tick_background);
                deleteItem.setWidth(150);
                deleteItem.setIcon(R.drawable.tick);
                menu.addMenuItem(deleteItem);
            }
        };
        lvReqFood.setMenuCreator(creator);
        lvReqFood.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {

                final Object id = (adapter.getItem(position));
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                alertDialog.setTitle("Confirm This Request?");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int item) {
                        adapter.updateSingleRequest(id.toString(), position, getContext());
                    }
                });
                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                alertDialog.show();
//                Toast.makeText(getContext(),"You clicked: " + id.toString(), Toast.LENGTH_SHORT).show();

                return false;
            }
        });
        return rootView;
    }

    private class LoadReqToConfirm extends AsyncTask<String, String, String > {
        ProgressDialog loadingRequests;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingRequests = new ProgressDialog(getContext());
            loadingRequests.setMessage("Loading...");
            loadingRequests.show();

        }

        @Override
        protected String doInBackground(String... strings) {
            String msg = "";
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("qc_id",qcId));
            JSONObject json = jParser.makeHttpRequest(url_req_to_confirm, "GET", params);
            Log.d("All Requests: ", json.toString());
            try {
                int success = json.getInt("success");

                if (success == 1) {

                    jaRequestedFood = json.getJSONArray("req_food");
                    if(jaRequestedFood.length() > 0) {
                        for (int i = 0; i < jaRequestedFood.length(); i++) {
                            noOfReq = jaRequestedFood.length();
                            JSONObject c = jaRequestedFood.getJSONObject(i);

                            String f_id = c.getString("f_id");
                            String req_id = c.getString("req_id");
                            String item_name = c.getString("item_name");
                            String type = c.getString("type");
                            String res_name = c.getString("name");
                            String prep_time = c.getString("prep_time");
                            String upload_time = c.getString("upload_time");
                            String req_status = c.getString("req_status");
                            String res_add = c.getString("add_line1");
                            Double qty = c.getDouble("req_qty");
                            String req_time = c.getString("req_time");


                            HashMap<String, String> map = new HashMap<String, String>();
                            map.put(KEY_FOOD_ID, f_id);
                            map.put(KEY_ID, req_id);
                            map.put(KEY_RES_NAME, res_name);
                            map.put(KEY_RES_ADD, res_add);
                            map.put(KEY_TYPE, type);
                            map.put(KEY_PREP_TIME, prep_time);
                            map.put(KEY_NAME, item_name);
                            map.put(KEY_UPLOAD_TIME, upload_time);
                            map.put(KEY_REQ_QTY, qty.toString());
                            map.put(KEY_REQ_TIME, req_time);
                            map.put(KEY_REQ_STATUS, req_status);

                            arrListReqFood.add(map);
                        }
                    }else{
                        noOfReq = 0;
                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
                msg = "Could not fetch requests";
            }catch (Exception e){
                msg = "Poor Internet Connection";
            }
            return msg;
        }

        @SuppressLint("RestrictedApi")
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            loadingRequests.dismiss();
            if(noOfReq == 0){
                imgNothingToShow.setVisibility(View.VISIBLE);
                btnConfirmAll.setVisibility(View.GONE);
                btnConfirMultiple.setVisibility(View.GONE);
            }else{
                imgNothingToShow.setVisibility(View.GONE);
                btnConfirmAll.setVisibility(View.VISIBLE);
                btnConfirMultiple.setVisibility(View.VISIBLE);
            }
            adapter = new ReqToConfirmAdapter(getActivity(), arrListReqFood);
            lvReqFood.setAdapter(adapter);
            btnConfirmAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                    alertDialog.setTitle("Are you sure?");
                    alertDialog.setMessage("Do you want to confirm all requests?");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int item) {
                            new UpdateRequest("All").execute();
                        }
                    });
                    alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    alertDialog.show();

                }
            });
            btnConfirMultiple.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                    alertDialog.setTitle("Confirm Selected Requests?");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int item) {
                            ArrayList<String> checkedList = adapter.getCheckedList();
                            try {
                                for (int i = 0; i < checkedList.size(); i++){
                                    temp.put(String.valueOf(i + 1), checkedList.get(i));
                                }

                                joAllRequests.put("requests",temp.toString());
                            }catch (JSONException j){
                                j.printStackTrace();
                            }
                            new UpdateRequest("Multiple").execute();
                        }
                    });
                    alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    alertDialog.show();


//                    Toast.makeText(getContext(), String.valueOf(checkedList.size()), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private class UpdateRequest extends AsyncTask<String, String, String>{
        String msg, type;

        public UpdateRequest(String type){
            this.type = type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            JSONParser jParserUpdate = new JSONParser();
            List<NameValuePair> requests = new ArrayList<NameValuePair>();
            if(type.equals("All")){
                requests.add(new BasicNameValuePair("All", "All"));
                requests.add(new BasicNameValuePair("qc",qcId));
                joAllRequests = jParserUpdate.makeHttpRequest(url_updt_food, "POST", requests);
            }else if(type.equals("Multiple")){
                requests.add(new BasicNameValuePair("qc",qcId));
                requests.add(new BasicNameValuePair("REQUESTS", joAllRequests.toString()));
                joAllRequests = jParserUpdate.makeHttpRequest(url_updt_food, "POST", requests);
            }
            Log.d("Request Status:",joAllRequests.toString());

            try{
                int success = joAllRequests.getInt("success");
                if(success == 1){
                    msg = "Requests Confirmed!";
                }else{
                    msg = "Error processing request";
                }
            }catch (JSONException j){
                j.printStackTrace();
                msg = "Could not update request";
            }catch (Exception e){
                msg = "Poor internet connection";
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
            if(msg.equals("Requests Confirmed!")) {
                if(type.equals("Multiple")) {
                    ArrayList<String> checkedList = adapter.getCheckedList();
                    for (int i = checkedList.size() - 1; i >= 0; i--) {
                        if (!checkedList.get(i).equals("0")) {
//                    String arrayTag[] = checkedList.get(i).split(",");
//                    int pos = Integer.parseInt(arrayTag[0]);
//                    Object toRemove = adapter.getItem(i);
//                    adapter.removeItem(toRemove)
                            adapter.removeItem(i);
                            checkedList.add(i,"0");
                        }
                    }
                    adapter.resetCheckedList();
                }else if(type.equals("All")){
                    adapter.clearAllData();
                }
            }
//            new LoadReqFood().execute();
        }
    }
}