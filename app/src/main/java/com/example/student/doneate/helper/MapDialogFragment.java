package com.example.student.doneate.helper;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.student.doneate.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.List;
import java.util.Locale;

import customfonts.MyTextView;

public class MapDialogFragment extends DialogFragment implements OnMapReadyCallback{
    Address address;
    StringBuffer addressDetails;
    List<Address> addresses;
    GoogleMap mMap;
    Marker marker;

    Location currentLocation;
    LatLng latLng;
    FusedLocationProviderClient fusedLocationProviderClient;
    private static final int REQUEST_CODE = 101;
    double latitude, longitude;
    String nAddLine2, receivedAddress;
    View rootView;
    MyTextView edAddress;
    Button btnSaveAdd;

    public static MapDialogFragment newInstance(int title) {
        MapDialogFragment frag = new MapDialogFragment();
        Bundle args = new Bundle();
        args.putInt("title", title);
        frag.setArguments(args);
        return frag;
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_map_dialog,container,false);
        edAddress = rootView.findViewById(R.id.etAddress);
        btnSaveAdd = rootView.findViewById(R.id.btnSaveAddress);
        btnSaveAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = getActivity().getIntent();
                i.putExtra("key_add", edAddress.getText().toString());
                i.putExtra("add_line2", nAddLine2);
                i.putExtra("latitude",latitude);
                i.putExtra("longitude",longitude);
                getTargetFragment().onActivityResult(getTargetRequestCode(), 101, i);
                getDialog().dismiss();
            }
        });
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());
        fetchLastLocation();
        return rootView;
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        latLng = new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude());
        receivedAddress = getAddress(currentLocation.getLatitude(), currentLocation.getLongitude());
        showResults(receivedAddress);
        mMap = googleMap;
        CameraPosition newCamPos = new CameraPosition(latLng,
                15.5f,
                mMap.getCameraPosition().tilt, //use old tilt
                mMap.getCameraPosition().bearing); //use old bearing
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCamPos), 4000, null);
//        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
//        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));
//        Toast.makeText(getContext(), latLng.toString(),Toast.LENGTH_LONG).show();

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                receivedAddress = getAddress(marker.getPosition().latitude, marker.getPosition().longitude);
                showResults(receivedAddress);
                mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));

            }
        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if(marker != null){
                    marker.remove();
                }
                marker = mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .draggable(true));
//                Toast.makeText(getContext(), latLng.toString(),Toast.LENGTH_SHORT).show();
                mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
                receivedAddress = getAddress(marker.getPosition().latitude, marker.getPosition().longitude);
                showResults(receivedAddress);
            }
        });
        marker = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(true));
    }

    private void fetchLastLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
            return;
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {

                    currentLocation = location;
//                    Toast.makeText(getContext(), location.toString(),Toast.LENGTH_LONG).show();
                    SupportMapFragment supportMapFragment = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.map);
                    supportMapFragment.getMapAsync(MapDialogFragment.this);
                }
            }
        });
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    fetchLastLocation();
                }
        }
    }

    public String getAddress(double latitude, double longitude){
        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());

        try{
//            latitude = marker.getPosition().latitude;
//            longitude = marker.getPosition().longitude;
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
        }catch (Exception IOException) {
            IOException.printStackTrace();
            Log.d("Error Message:", IOException.getMessage());
            Log.e("", "Error in getting address for the location");
            Toast.makeText(getContext(), "Address not found",Toast.LENGTH_SHORT).show();
        }

        if(addresses == null || addresses.size() == 0){
            Log.e("","Error in getting address for the location");

        }
        address = addresses.get(0);
        addressDetails = new StringBuffer();

        //0
        addressDetails.append(String.valueOf(latitude));
        addressDetails.append("\n");
        //1
        addressDetails.append(String.valueOf(longitude));
        addressDetails.append("\n");
        //2
        for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
            addressDetails.append(address.getAddressLine(i));
            addressDetails.append(",");
        }
        addressDetails.append("\n");
//            //3
//            if(address.getThoroughfare() != null){
//                addressDetails.append("ThoroughFare " + address.getThoroughfare());
//            }else{
//                addressDetails.append("");
//            }
//            addressDetails.append("\n");
//            //4
//            if(address.getSubLocality() != null){
//                addressDetails.append("Locality " + address.getLocality());
//            }else{
//                addressDetails.append("");
//            }
//            addressDetails.append("\n");
//            //5
//            if(address.getSubLocality() != null){
//                addressDetails.append("Sublocality " + address.getSubLocality());
//            }else{
//                addressDetails.append("");
//            }
//            addressDetails.append("\n");
//            //6
//            if(address.getFeatureName() != null){
//                addressDetails.append("Feature " + address.getFeatureName());
//            }else{
//                addressDetails.append("");
//            }
//            3 addressDetails.append("City:");
        addressDetails.append(address.getLocality());
        addressDetails.append("\n");

//                      4 addressDetails.append("State:");
        addressDetails.append(address.getAdminArea());
        addressDetails.append("\n");

//                      5 addressDetails.append("Postal Code: ");
        addressDetails.append(address.getPostalCode());
        addressDetails.append("\n");

        return addressDetails.toString();

    }

    private void showResults(String currentAdd) {
        String[] address  = currentAdd.split("\n");
//        Toast.makeText(getContext(), String.valueOf(address.length), Toast.LENGTH_LONG).show();
        if(!currentAdd.equals("No address found!")) {
            latitude = Double.parseDouble(address[0]);
            longitude = Double.parseDouble(address[1]);

            edAddress.setText(address[2]);
            nAddLine2 = address[3] + "," + address[4] + "," + address[5];
        }
    }

    @Override
    public void onDestroyView() {
        try {
            SupportMapFragment supportMapFragment = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.map);
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.remove(supportMapFragment);
            fragmentTransaction.commit();
        }catch (Exception e){

        }
        super.onDestroyView();
    }
}
