package com.example.student.doneate.restaurant;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.student.doneate.R;
import com.example.student.doneate.helper.JSONParser;
import com.example.student.doneate.helper.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;


public class ResProfile extends Fragment {

    private static String url_res_details = "http://10.112.68.50/doneate/get_res_details.php";
    private static String serverUrl = "https://doneate.000webhostapp.com/get_res_details.php";

    private static String url_update_image = "http://10.112.68.50/doneate/update_image.php";
    private static String serverUrlImage = "https://doneate.000webhostapp.com/update_image.php";
    static final int RESULT_LOAD_IMAGE = 1;
    JSONParser jsonParser = new JSONParser();
    JSONParser imgParser = new JSONParser();
    TextView tvName, tvLocation, tvUsername, tvPhone,tvAddress,tvCategory,tvRating;
    ImageView btnEdit, btnEditPic, profilePic;
    Bitmap image,receivedBitmap;
    String name, city, username, phone, address, category, photo_url;
    String resID, userId;
    Double rating;
    View rootview;
    public SessionManager userSession;

    public ResProfile() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_res_profile, container, false);
        tvName = rootview.findViewById(R.id.name);
        tvLocation = rootview.findViewById(R.id.location);
        tvUsername = rootview.findViewById(R.id.Username);
        tvPhone = rootview.findViewById(R.id.tvPhone);
        tvAddress = rootview.findViewById(R.id.tvAddress);
        tvCategory = rootview.findViewById(R.id.tvCompany);
        tvRating = rootview.findViewById(R.id.tvLicenseNo);
        profilePic = rootview.findViewById(R.id.profile_pic);
        btnEditPic = rootview.findViewById(R.id.profile_pic_edit);
        btnEditPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent,RESULT_LOAD_IMAGE);
            }
        });
        GetData getResData = new GetData();
        getResData.execute();
        return rootview;
    }

//    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }

    private class GetData extends AsyncTask<String, String, String>{
        String msg;
        ProgressDialog loading = new ProgressDialog(getContext());
        @Override
        protected void onPreExecute(){
            loading.setMessage("Loading Profile..");
            loading.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try{
                userSession = new SessionManager(getContext());
                userId = userSession.getUserId();
                int success;
                List<NameValuePair> args = new ArrayList<NameValuePair>();
                args.add(new BasicNameValuePair("r_id",userId));

                JSONObject json = jsonParser.makeHttpRequest(
                        url_res_details, "GET", args);
                Log.d("Restaurant Details", json.toString());
                success = json.getInt("success");
                if(success == 1){

                    JSONArray restaurantObj = json.getJSONArray("restaurant");
                    JSONObject restaurant = restaurantObj.getJSONObject(0);
                    name = restaurant.getString("name");
                    username = restaurant.getString("uname");
                    phone = restaurant.getString("phn_no");
                    address = restaurant.getString("add_line1");
                    city = restaurant.getString("add_line2");
                    rating = restaurant.getDouble("g_rating");
                    category = restaurant.getString("category");
                    photo_url = restaurant.getString("photo");
                }else{
                    msg = "No restaurant found!";
                }
            }catch(JSONException j){
                msg = j.getMessage().toString();
            }catch (Exception e){
                msg = "Poor internet connection";
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            loading.dismiss();
//            if(s != null){
                int idx = city.indexOf(",");
                city = city.substring(0,idx);
                tvName.setText(name);
                tvLocation.setText(city);
                tvUsername.setText(username);
                tvAddress.setText(address);
                tvPhone.setText(phone);
                tvCategory.setText(category);
                tvRating.setText(String.valueOf(rating));
                new GetImage("res_images/IMG" + userId + ".JPG").execute();
//            }
            super.onPostExecute(s);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null ){
            Uri selectedImage = data.getData();
            profilePic.setImageURI(selectedImage);
            image = ((BitmapDrawable)profilePic.getDrawable()).getBitmap();
            SessionManager sessionManager = new SessionManager(getContext());
            resID = sessionManager.getUserId();
            UploadImage uploadImage = new UploadImage(image,"IMG" + resID);
            uploadImage.execute();
        }
    }

    private class UploadImage extends AsyncTask<String , String , String>{
        Bitmap image;
        String name, msg;

        public UploadImage(Bitmap image, String name){
            this.image = image;
            this.name = name;
        }
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected String doInBackground(String... strings) {

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
            String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("image",encodedImage));
            params.add(new BasicNameValuePair("name",name));
            params.add(new BasicNameValuePair("r_id",resID));
            JSONObject json = imgParser.makeHttpRequest(url_update_image,"POST", params);
            Log.d("Create Response", json.toString());
            try {
                int success = json.getInt("success");

                if (success == 1) {
                    msg = "Uploaded Successfully";

                } else {
                    msg = "Couldn't Upload Image";
                }
            } catch (JSONException e) {
                msg = e.getMessage().toString();
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String msg){
            ImageView mLogo = ((ResHomeActivity)getActivity()).findViewById(R.id.ivNavRes);
            mLogo.setImageBitmap(image);
        }
    }

    private class GetImage extends AsyncTask<Void, Void, Bitmap>{
        String name;
        public GetImage(String name){
            this.name = name;
        }
        @Override
        protected Bitmap doInBackground(Void... voids) {
            String serverGetPhoto = "https://doneate.000webhostapp.com/res_images/IMG"+ resID + ".JPG";
            photo_url = "http://10.112.68.50/doneate/" + photo_url;
            try{
                URLConnection connection = new URL(photo_url).openConnection();
                connection.setConnectTimeout(1000 * 30);
                connection.setReadTimeout(1000 * 30);
                receivedBitmap = BitmapFactory.decodeStream((InputStream)connection.getContent(),null,null);
                return receivedBitmap;
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Bitmap bitmap){
            super.onPostExecute(bitmap);
            if(bitmap != null){
                ImageView mLogo = ((ResHomeActivity)getActivity()).findViewById(R.id.ivNavRes);
                mLogo.setImageBitmap(receivedBitmap);
                profilePic.setImageBitmap(receivedBitmap);
            }
        }
    }
}
