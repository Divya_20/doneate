package com.example.student.doneate.helper;

import android.app.Activity;
import android.os.AsyncTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CheckUserExists extends AsyncTask<String, String, String> {
    String urlCheckUserExists = "http://10.112.68.50/doneate/check_user.php";
    String serverUrlUserExists = "https://doneate.000webhostapp.com/check_user.php";
    JSONParser jsonParser = new JSONParser();
    String username;
    Activity activity;

    public static boolean userExists = false;
    String msg;
    public CheckUserExists(String username){
        this.username = username;
//        this.activity = activity;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... strings) {
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("u_name",username));
        JSONObject jsonObject = jsonParser.makeHttpRequest(urlCheckUserExists,"GET",pairs);
        try {
            int success = jsonObject.getInt("success");

            if (success == 1) {
                msg = "User Already Exists";
                userExists = true;

            } else {
                msg = jsonObject.getString("error");
            }
        } catch (JSONException e) {
            msg = e.getMessage();
            e.printStackTrace();
        }
        return msg;
    }

    @Override
    protected void onPostExecute(String s) {
       if(s.equals("User Already Exists") ){
           userExists = true;
       }else{
           userExists = false;
       }
        super.onPostExecute(s);
    }
    public boolean getUserExists(){
        return userExists;
    }
}
