package com.example.student.doneate.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.student.doneate.R;
import com.example.student.doneate.helper.GMailSender;
import com.example.student.doneate.helper.JSONParser;
import com.example.student.doneate.helper.MD5;
import com.example.student.doneate.helper.SessionManager;
import com.example.student.doneate.ngo.NgoHomeActivity;
import com.example.student.doneate.qc.QcDefaultActivity;
import com.example.student.doneate.restaurant.ResHomeActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import customfonts.MyEditText;
import customfonts.MyTextView;

public class SignInActivity extends AppCompatActivity {

    JSONParser jsonParser = new JSONParser();
    public SessionManager userSession;
    public boolean isValidUser;

    ImageView back;
    Button btnSignIn;
    TextView tvForgotPwd;
    MyTextView tvTimer;
    MyEditText uname, pwd;
    String msg = ""; String res;

    EditText edOTP;
    Button btnConfirm;
    public String username, password, encPwd, uid, userType;
    public int auth_status;
    private static String url_check_user = "http://10.112.68.50/doneate/check_user.php";
    private static String serverUrl = "https://doneate.000webhostapp.com/check_user.php";
    private String sentOTP;
    private String receivedOTP;
//    String ServerURL = "jdbc:mysql://10.112.68.50/doneate";
//    private static final String USER = "doneate";
//    private static final String PASS = "doneate";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        uname = findViewById(R.id.username);
        pwd = findViewById(R.id.password);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent itBackToMain = new Intent(SignInActivity.this, MainActivity.class);
                startActivity(itBackToMain);
            }
        });
        btnSignIn = findViewById(R.id.btn_sign_in);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            boolean flag = true;
            @Override
            public void onClick(View v) {
                username = uname.getText().toString().trim();
                password = pwd.getText().toString().trim();

                if(username.isEmpty() || password.isEmpty()){
                    uname.setError("Username empty!");
                    flag = false;
                }
                if(password.isEmpty()){
                    pwd.setError("Password empty!");
                    flag = false;
                }

                if(flag){
                    Send verifyLogin = new Send();
                    verifyLogin.execute();

                }
            }
        });

        tvForgotPwd = findViewById(R.id.tvForgotPwd);
        tvForgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = uname.getText().toString().trim();
                if(username.equals("") || username == null){
                    Toast.makeText(getApplicationContext(), "Please enter the email id above", Toast.LENGTH_LONG).show();
                    uname.setError("Enter your email to continue");
                }else{
                    new CheckUserExists().execute();

                }
            }
        });
    }

    public String generateOTP(){
        String strOTP = "";
        String numbers = "0123456789";
        Random rndm_method = new Random();
        for (int i = 0; i < 6; i++){
            strOTP = strOTP + numbers.charAt(rndm_method.nextInt(numbers.length()));
        }
        return strOTP;
    }

    public void checkEmailCode(final String tempPwd){
        final boolean[] isOTPExpired = new boolean[1];
        ViewGroup viewGroup = findViewById(R.id.sign_in_content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_verify, viewGroup,false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);

        final AlertDialog alert = builder.create();
        alert.show();
        edOTP = dialogView.findViewById(R.id.editTextOtp);
        tvTimer = dialogView.findViewById(R.id.tvTimer);
        tvTimer.setVisibility(View.VISIBLE);
        final int time = 300000;
        new CountDownTimer(300000, 1000) {
            int timeChange = time;
            boolean OTPExpired;
            public void onTick(long millisUntilFinished) {
                tvTimer.setText("" + String.format("%d:%d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                timeChange--;
            }

            public void onFinish() {
                alert.dismiss();
                isOTPExpired[0] = true;
                Toast.makeText(getApplicationContext(), "OTP expired", Toast.LENGTH_SHORT).show();
            }

        }.start();


        btnConfirm = dialogView.findViewById(R.id.btnConfirmOTP);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                receivedOTP = edOTP.getText().toString().trim();

                final ProgressDialog authenticating = ProgressDialog.show(SignInActivity.this, "Authenticating", "Please wait while we check the entered code", false,false);
                authenticating.show();
                Handler sender = new Handler(Looper.getMainLooper());
                {
                    sender.post(new Runnable() {
                        @Override
                        public void run() {
                            if (receivedOTP.equals(tempPwd) && !isOTPExpired[0]) {
                                authenticating.dismiss();
                                alert.dismiss();
                                changePassword();
                            } else {
                                Toast.makeText(getApplicationContext(), "Wrong OTP or OTP expired", Toast.LENGTH_LONG).show();
                                try {
                                    authenticating.dismiss();
                                    alert.dismiss();
                                    //Asking user to enter otp again
                                    checkEmailCode(tempPwd);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                }
            }
        });
    }

    private void changePassword() {

        Intent inChangePwd = new Intent(SignInActivity.this, ChangePassword.class);
        inChangePwd.putExtra("uname", username);
        inChangePwd.putExtra("utype", userType);
        startActivity(inChangePwd);
    }


    private class SendEmail extends AsyncTask<String, String, String> {
        ProgressDialog loading;
        String mail, tempPwd;

        public SendEmail(String mail, String tempPwd) {
            this.mail = mail;
            this.tempPwd = tempPwd;
        }

        @Override
        protected void onPreExecute(){
            loading = ProgressDialog.show(SignInActivity.this, "Sending Mail", "Please wait...", false, false);
            loading.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String msg = "";
            try {
                GMailSender sender = new GMailSender("doneate.app@gmail.com", "done@ate");
                if(sender.sendMail("OTP from DoneAte",
                        "This is your temporary Password. Enter it on the next screen to change your password." +
                                "\n <strong> This OTP will expire in 2 minutes.</strong>  \n" + tempPwd,
                        "doneate.app@gmail.com", mail)) {
                    msg = "Email Sent";
                }
            } catch (Exception e) {
                msg = e.getMessage();
            }
            return msg;
        }
        @Override
        protected void onPostExecute(String msg){
            loading.dismiss();
            if(msg.equals("Email Sent")){
                checkEmailCode(tempPwd);
            }else{
                Toast.makeText(getApplicationContext(),"Error sending mail, Verify your email address",Toast.LENGTH_LONG).show();
            }
        }
    }

    private class Send extends AsyncTask<String, String, String> {
        ProgressDialog signingIn;
        @Override
        protected void onPreExecute(){
            signingIn = new ProgressDialog(SignInActivity.this);
            signingIn.setMessage("Signing in...");
            signingIn.show();
        }
        @Override
        protected String doInBackground(String... params){
            try{
                encPwd = MD5.getHash(password);
                int success;
                List<NameValuePair> args = new ArrayList<NameValuePair>();
                args.add(new BasicNameValuePair("u_name",username));
                args.add(new BasicNameValuePair("u_pwd",encPwd));
                JSONObject json = jsonParser.makeHttpRequest(
                        url_check_user, "GET", args);
                Log.d("User Details", json.toString());
                success = json.getInt("success");
                if(success == 1){
                    userSession = new SessionManager(getApplicationContext());
                    JSONArray userObj = json.getJSONArray("user");
                    JSONObject user = userObj.getJSONObject(0);
                    auth_status = user.getInt("u_status");
                    uid = user.getString("u_id");
                    userType = user.getString("u_type");
                    username = user.getString("u_name");
                    if(auth_status == 1){
                        userSession.setUserId(uid);
                        userSession.setUsername(username);
                        userSession.setUserType(userType);
                        isValidUser = true;
                    }else{
                        res = "Your credentials are not yet verified";
                        isValidUser = false;
                    }
                }else{
                    res = "Invalid credentials";
                    isValidUser = false;
                }
            }catch(JSONException j){
                msg = j.getMessage().toString();
            }
//            try{
////                DBInteraction dbInteraction = new DBInteraction();
////                Connection conn =  dbInteraction.getConn();
//                Class.forName("com.mysql.jdbc.Driver");
//                Connection conn = DriverManager.getConnection(ServerURL,USER,PASS);
//                if(conn == null){
//                    msg = "Connection goes null";
//                }else {
//                    String Qry = "SELECT * FROM user_role WHERE u_name = ? and u_pwd = ? ";
//                    encPwd = MD5.getHash(password);
//                    PreparedStatement selQuery = conn.prepareStatement(Qry);
//                    selQuery.setString(1,username);
//                    selQuery.setString(2,encPwd);
//                    ResultSet rs = selQuery.executeQuery();
//                    if(!rs.isBeforeFirst()){
//                        res = "Invalid Login Credentials";
//                    }else {
//                        userSession = new SessionManager(getApplicationContext());
//                        while (rs.next()) {
//                            uid = rs.getString("u_id");
//                            auth_status = rs.getInt("u_status");
//                            userType = rs.getString("u_type");
////                            res = uid + " " + auth_status;
////                            isValidUser = true;
//                        }
//                        if(auth_status == 1) {
//                            userSession.setUserId(uid);
//                            userSession.setUsername(username);
//                            userSession.setUserType(userType);
//                            isValidUser = true;
//                        }else{
//                            res = "Your credentials are not yet verified";
//                            isValidUser = false;
//                        }
//                    }
//                    conn.close();
//                }
//            }catch (Exception e){
//                msg = "Connection goes wrong";
//                msg = e.toString();
//                e.printStackTrace();
//            }
            return msg;
        }

        @Override
        protected void onPostExecute(String msg){
//            Toast.makeText(getApplicationContext(), res, Toast.LENGTH_LONG).show();
            if(isValidUser) {
                signingIn.dismiss();
                switch (userType){
                    case "Restaurant":
                        Intent itResHome = new Intent(SignInActivity.this, ResHomeActivity.class);
                        startActivity(itResHome);
                        break;
                    case"NGO":
                        Intent itNgoHome = new Intent(SignInActivity.this, NgoHomeActivity.class);
                        startActivity(itNgoHome);
                        break;
                    case"QC":
                        Intent itQCHome = new Intent(SignInActivity.this, QcDefaultActivity.class);
                        startActivity(itQCHome);
                        break;
                }

            }else{
                signingIn.dismiss();
                Toast.makeText(getApplicationContext(), res, Toast.LENGTH_LONG).show();
            }
        }
    }

    private class CheckUserExists extends AsyncTask<String, String, String> {
        ProgressDialog verifying;
        @Override
        protected void onPreExecute(){
            verifying = new ProgressDialog(SignInActivity.this);
            verifying.setMessage("Verifying...");
            verifying.show();
        }
        @Override
        protected String doInBackground(String... params){
            try{
                int success;
                List<NameValuePair> args = new ArrayList<NameValuePair>();
                args.add(new BasicNameValuePair("u_name",username));
                JSONObject json = jsonParser.makeHttpRequest(
                        url_check_user, "GET", args);
                Log.d("User Details", json.toString());
                success = json.getInt("success");
                if(success == 1){
                    JSONArray userObj = json.getJSONArray("user");
                    JSONObject user = userObj.getJSONObject(0);
                    uid = user.getString("u_id");
                    userType = user.getString("u_type");
                    isValidUser = true;
                }else{
                    res = "Email does not exist";
                    isValidUser = false;
                }
            }catch(JSONException j){
                msg = j.getMessage().toString();
            }
//            try{
////                DBInteraction dbInteraction = new DBInteraction();
////                Connection conn =  dbInteraction.getConn();
//                Class.forName("com.mysql.jdbc.Driver");
//                Connection conn = DriverManager.getConnection(ServerURL,USER,PASS);
//                if(conn == null){
//                    msg = "Connection goes null";
//                }else {
//                    String Qry = "SELECT * FROM user_role WHERE u_name = ? and u_pwd = ? ";
//                    encPwd = MD5.getHash(password);
//                    PreparedStatement selQuery = conn.prepareStatement(Qry);
//                    selQuery.setString(1,username);
//                    selQuery.setString(2,encPwd);
//                    ResultSet rs = selQuery.executeQuery();
//                    if(!rs.isBeforeFirst()){
//                        res = "Invalid Login Credentials";
//                    }else {
//                        userSession = new SessionManager(getApplicationContext());
//                        while (rs.next()) {
//                            uid = rs.getString("u_id");
//                            auth_status = rs.getInt("u_status");
//                            userType = rs.getString("u_type");
////                            res = uid + " " + auth_status;
////                            isValidUser = true;
//                        }
//                        if(auth_status == 1) {
//                            userSession.setUserId(uid);
//                            userSession.setUsername(username);
//                            userSession.setUserType(userType);
//                            isValidUser = true;
//                        }else{
//                            res = "Your credentials are not yet verified";
//                            isValidUser = false;
//                        }
//                    }
//                    conn.close();
//                }
//            }catch (Exception e){
//                msg = "Connection goes wrong";
//                msg = e.toString();
//                e.printStackTrace();
//            }
            return msg;
        }

        @Override
        protected void onPostExecute(String msg){
            verifying.dismiss();
            if(isValidUser) {
                sentOTP = generateOTP();
                String mail = uname.getText().toString();
                new SendEmail(mail, sentOTP).execute();
            }else{
                Toast.makeText(getApplicationContext(), "User does not exist", Toast.LENGTH_LONG).show();
            }
        }
    }

}
