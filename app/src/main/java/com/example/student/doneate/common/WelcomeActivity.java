package com.example.student.doneate.common;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.student.doneate.ngo.NgoHomeActivity;
import com.example.student.doneate.R;
import com.example.student.doneate.qc.QcDefaultActivity;
import com.example.student.doneate.restaurant.ResHomeActivity;
import com.example.student.doneate.helper.SessionManager;

public class WelcomeActivity extends AppCompatActivity {
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 2;
    private static int WELCOME_TIME_OUT = 2000;
    String ServerURL = "jdbc:mysql://10.112.68.50/doneate";
    private static final String USER = "doneate";
    private static final String PASS = "doneate";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        fetchLocationPermission();
    }

    public void fetchLocationPermission() {
        if (ContextCompat.checkSelfPermission(WelcomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(WelcomeActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},LOCATION_PERMISSION_REQUEST_CODE);
//            return false;
            // Permission is not granted
            // Should we show an explanation?
//            if (ActivityCompat.shouldShowRequestPermissionRationale(WelcomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
//                new AlertDialog.Builder(this)
//                        .setTitle("Permission Required")
//                        .setMessage("Kindly allow to access location  to continue")
//                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                ActivityCompat.requestPermissions(WelcomeActivity.this,
//                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                                        LOCATION_PERMISSION_REQUEST_CODE);
//                            }
//                        })
//                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                                finishAffinity();
//                            }
//                        })
//                        .create()
//                        .show();
//            } else {
//                // No explanation needed; request the permission
////                ActivityCompat.requestPermissions(WelcomeActivity.this,
////                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
////                        LOCATION_PERMISSION_REQUEST_CODE);
//
//                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
//                // app-defined int constant. The callback method gets the
//                // result of the request.
//            }
        } else {
            // Permission has already been granted
            startApp();

        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startApp();
//                    startLocationUpdates();
                } else {
//                    Toast.makeText(this, "PERMISSION DENIED", Toast.LENGTH_SHORT).show();
                    new AlertDialog.Builder(this)
                        .setTitle("Permission Required")
                        .setMessage("Kindly allow location access to continue")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(WelcomeActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        LOCATION_PERMISSION_REQUEST_CODE);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finishAffinity();
                            }
                        })
                        .create()
                        .show();
                }
                return;
            }

        }
    }
    public void startApp(){

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SessionManager checkUserSessionManager = new SessionManager(getApplicationContext());
//                String username = checkUserSessionManager.getUsername();
                String userId = checkUserSessionManager.getUserId();//check user type using userid and redirect the user to his home page
                String userType = checkUserSessionManager.getUserType();
                if(userId != null && (!userId.equals(""))) {
                    switch (userType) {
                        case "Restaurant":
                            Intent resHome = new Intent(WelcomeActivity.this, ResHomeActivity.class);
                            startActivity(resHome);
                            break;
                        case"NGO":
                            Intent itNgoHome = new Intent(WelcomeActivity.this, NgoHomeActivity.class);
                            startActivity(itNgoHome);
                            break;
                        case"QC":
                            Intent itQCHome = new Intent(WelcomeActivity.this, QcDefaultActivity.class);
                            startActivity(itQCHome);
                            break;
                    }
                }
                else{
                    Intent mainIntent = new Intent(WelcomeActivity.this, MainActivity.class);
                    startActivity(mainIntent);
                }
            }
        },WELCOME_TIME_OUT);
    }


}
