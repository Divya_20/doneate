package com.example.student.doneate.ngo;

import java.util.Date;

public class Food {
    String foodId, resId;
    String itemName, type, uploadTime, lat, longi, rating, resName;
    Double qty;
    Date prepTime;

    public Food(String foodId, String resId, String itemName, String type, Date prepTime, String uploadTime, String lat, String longi, String rating, String resName, Double qty) {
        this.foodId = foodId;
        this.itemName = itemName;
        this.type = type;
        this.prepTime = prepTime;
        this.uploadTime = uploadTime;
        this.lat = lat;
        this.longi = longi;
        this.rating = rating;
        this.resName = resName;
        this.qty = qty;
        this.resId = resId;
    }

    public String getFoodId() {
        return foodId;
    }

    public String getItemName() {
        return itemName;
    }

    public String getType() {
        return type;
    }

    public Date getPrepTime() {
        return prepTime;
    }

    public String getUploadTime() {
        return uploadTime;
    }

    public String getLat() {
        return lat;
    }

    public String getLongi() {
        return longi;
    }

    public String getRating() {
        return rating;
    }

    public String getResName() {
        return resName;
    }

    public Double getQty() {
        return qty;
    }

//    public Food getItemAtPosition(int position){
//
//    }

}
