package com.example.student.doneate.ngo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.student.doneate.R;
import com.example.student.doneate.common.SignInActivity;
import com.example.student.doneate.helper.JSONParser;
import com.example.student.doneate.helper.MD5;
import com.example.student.doneate.helper.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NgoHomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AddQcPerson.GetQcPersonData {
    DrawerLayout drawer;
    Button btnLogout, btnCancel;
    NavigationView navigationView;
    String QcId, QcName, QcUname, QcPwd, QcQual, QcCnfPwd, QcGender, QcPhone;
    String userId, name, username, photo_url;
    private static String url_register_qc = "http://10.112.68.50/doneate/register_qc.php";
    private static String serverUrl_register_qc = "https://doneate.000webhostapp.com/register_qc.php";

    private static String url_ngo_details = "http://10.112.68.50/doneate/get_ngo_details.php";
    private static String serverUrl = "https://doneate.000webhostapp.com/get_ngo_details.php";

    private static String url_check_qc_avl = "http://10.112.68.50/doneate/check_qc_available.php";
    private static String serverUrlQcAvl = "https://doneate.000webhostapp.com/check_qc_available.php";

    JSONParser jsonParser = new JSONParser();
    String ngoId;
    String encPwd;
    ImageView ivNavNGO;
    TextView tvNavNGOName, tvNavNGOUname, tvngoWelcome;
    Bitmap receivedBitmap;
    boolean isQcAdded;
    BottomNavigationView bottomNavigationView;
//    LinearLayout LayoutAddQc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ngo_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        bottomNavigationView = findViewById(R.id.bottom_nav);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        View layoutNavHeader = navigationView.getHeaderView(0);

        ivNavNGO = layoutNavHeader.findViewById(R.id.ivNavNGO);
        tvNavNGOUname = layoutNavHeader.findViewById(R.id.tvNavNgoUname);
        tvNavNGOName  = layoutNavHeader.findViewById(R.id.tvNavNgoName);
        SessionManager checkUserSessionManager = new SessionManager(getApplicationContext());
        String username = checkUserSessionManager.getUsername();
        userId = checkUserSessionManager.getUserId();
        String userType = checkUserSessionManager.getUserType();
        if(userId != null && (!userId.equals(""))) {
            navigationView.getMenu().findItem(R.id.nav_ngo_login).setVisible(false);
            new GetData().execute();
            new CheckQcAdded().execute();
        }else{
            navigationView.getMenu().findItem(R.id.nav_ngo_logout).setVisible(false);
        }
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
//        if(isQcAdded) {
//            LayoutAddQc.setVisibility(View.GONE);
//            getMenuInflater().inflate(R.menu.ngo_home, menu);
//            FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
//            tx.replace(R.id.frames_ngo, new AvailableFood());
//            tx.commit();
//        }else{
////            LayoutAddQc.setVisibility(View.VISIBLE);
////            tvngoWelcome.setText(name);
//        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        selectItemDrawer(item);
        int id = item.getItemId();


//        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void selectItemDrawer(MenuItem menuItem){
        Fragment fragment = null;
        Class FragmentClass = null;

        switch (menuItem.getItemId()){
            case R.id.nav_view_avl_food:
                FragmentClass = AvailableFood.class;
                break;
            case R.id.nav_current_req:
                FragmentClass = CurrentRequestsNGO.class ;
                break;
            case R.id.nav_add_qc:
                if(isQcAdded){
                    FragmentClass = ShowAllQC.class;
                }else{
                    FragmentClass = AddQcPerson.class;
                }

                break;
//            case R.id.nav_donated_food:
//                FragmentClass = DonatedFoodNGO.class;
//                break;
            case R.id.nav_ngo_profile:
                FragmentClass = NgoProfile.class;
                break;
            case R.id.nav_ngo_logout:
                FragmentClass = NgoProfile.class;
                showPopup();
                break;
            case R.id.nav_ngo_login:
                Intent itSignIn = new Intent(NgoHomeActivity.this, SignInActivity.class);
                startActivity(itSignIn);

        }
        try{
            fragment = (Fragment)FragmentClass.newInstance();
        }catch (Exception e){
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frames_ngo, fragment).commit();
        menuItem.setChecked(true);
        setTitle(menuItem.getTitle());
        getSupportActionBar().setTitle(menuItem.getTitle());
        drawer.closeDrawers();
    }

    public void setActionBarTitle(String title){
        getSupportActionBar().setTitle(title);
    }

    private void showPopup() {
        ViewGroup viewGroup = findViewById(R.id.frames_ngo);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog, viewGroup, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);

        final AlertDialog alert = builder.create();
        alert.show();
        btnLogout = dialogView.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionManager clearSessionManager = new SessionManager(getApplicationContext());
                clearSessionManager.clearAll();

                Intent loginIntent = new Intent(NgoHomeActivity.this, SignInActivity.class);
                loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loginIntent);
                finish();
            }
        });
        btnCancel = dialogView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
    }

    @Override
    public void onGetQcPersonData(String name, String uname, String phone, String pass, String qual, String gender) {
        QcName = name; QcUname = uname; QcPhone = phone; QcPwd = pass; QcQual = qual; QcGender = gender;
    }

    public void registerQc(){
        String ts = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        SessionManager getNgo = new SessionManager(getApplicationContext());
        ngoId = getNgo.getUserId();
        String ngoName = getNgo.getUsername();
        QcId = "Q" + ngoName.substring(0,3) + ngoId.substring(1,8) + ts;
        encPwd = MD5.getHash(QcPwd);
        new SendQcData().execute();
    }

    private class SendQcData extends AsyncTask<String, String, String>{
        String msg = "";
        ProgressDialog registering;

        @Override
        protected void onPreExecute(){
            registering = new ProgressDialog(NgoHomeActivity.this);
            registering.setMessage("Registering...");
            registering.setIndeterminate(false);
            registering.setCancelable(false);
            registering.show();
        }
        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("qc_id",QcId));
            pairs.add(new BasicNameValuePair("n_id", ngoId));
            pairs.add(new BasicNameValuePair("name", QcName));
            pairs.add(new BasicNameValuePair("uname",QcUname));
            pairs.add(new BasicNameValuePair("gender",QcGender));
            pairs.add(new BasicNameValuePair("phone",QcPhone));
            pairs.add(new BasicNameValuePair("pwd",encPwd));
            pairs.add(new BasicNameValuePair("qualification",QcQual));
            pairs.add(new BasicNameValuePair("dec_pwd", QcPwd));

            JSONObject jsonObject = jsonParser.makeHttpRequest(url_register_qc,"POST",pairs);
            try {
                int success = jsonObject.getInt("success");

                if (success == 1) {
                    msg = "Registered Successfully";

                } else {
                    msg = "Couldn't Add QC";
                }
            } catch (JSONException e) {
                msg = e.getMessage().toString();
                e.printStackTrace();
            }

            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            registering.dismiss();
            Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
        }
    }

    private class GetData extends AsyncTask<String, String, String>{
        String msg;
        @Override
        protected void onPreExecute(){

        }

        @Override
        protected String doInBackground(String... strings) {
            try{

                int success;
                List<NameValuePair> args = new ArrayList<NameValuePair>();
                args.add(new BasicNameValuePair("n_id",userId));

                JSONObject json = jsonParser.makeHttpRequest(url_ngo_details
                        , "GET", args);
                Log.d("NGO Details", json.toString());
                success = json.getInt("success");
                if(success == 1){

                    JSONArray NGOObj = json.getJSONArray("restaurant");
                    JSONObject NGO = NGOObj.getJSONObject(0);
                    name = NGO.getString("name");
                    username = NGO.getString("uname");
                    photo_url = NGO.getString("photo");
                }else{
                    msg = "No NGO found!";
                }
            }catch(JSONException j){
                msg = j.getMessage().toString();
            }catch (Exception e){
                msg = "Poor internet connection";
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            tvNavNGOName.setText(name);
            tvNavNGOUname.setText(username);
            new GetImage("ngo_images/IMG" + userId + ".JPG").execute();
            super.onPostExecute(s);
        }
    }

    private class GetImage extends AsyncTask<Void, Void, Bitmap>{
        String name;
        public GetImage(String name){
            this.name = name;
        }
        @Override
        protected Bitmap doInBackground(Void... voids) {
            String serverGetPhoto = "https://doneate.000webhostapp.com/ngo_images/IMG"+ userId + ".JPG";
            photo_url = "http://10.112.68.50/doneate/" + name;
            try{
                URLConnection connection = new URL(photo_url).openConnection();
                connection.setConnectTimeout(1000 * 30);
                connection.setReadTimeout(1000 * 30);
                receivedBitmap = BitmapFactory.decodeStream((InputStream)connection.getContent(),null,null);
                return receivedBitmap;
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Bitmap bitmap){
            super.onPostExecute(bitmap);
            if(bitmap != null){
                ivNavNGO.setImageBitmap(receivedBitmap);
            }
        }
    }

    private class CheckQcAdded extends AsyncTask<String, String, String>{
        String msg;
        @Override
        protected String doInBackground(String... strings) {
            try{

                int success;
                List<NameValuePair> args = new ArrayList<NameValuePair>();
                args.add(new BasicNameValuePair("n_id",userId));

                JSONObject json = jsonParser.makeHttpRequest(url_check_qc_avl
                        , "GET", args);
                Log.d("Qc Available", json.toString());
                success = json.getInt("success");
                if(success == 1){
                    isQcAdded = true;
                    new SessionManager(getApplicationContext()).setQcAdded(true);
                }else{
                    isQcAdded = false;
                    msg = "No Qc found!";
                }
            }catch(JSONException j){
                msg = j.getMessage().toString();
            }catch (Exception e){
                msg = "Poor internet connection";
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(isQcAdded){
//                LayoutAddQc.setVisibility(View.GONE);
//                getMenuInflater().inflate(R.menu.ngo_home, menu);
                FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.frames_ngo, new AvailableFood());
                tx.commit();
            }else{
                NgoWelcome fragment = new NgoWelcome();
                Bundle bundle = new Bundle();
                bundle.putString("ngoName", name);
                fragment.setArguments(bundle);
                FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.frames_ngo, fragment);
                tx.commit();
//                LayoutAddQc.setVisibility(View.VISIBLE);
//                tvngoWelcome.setText(name);

//
            }
        }
    }
}
