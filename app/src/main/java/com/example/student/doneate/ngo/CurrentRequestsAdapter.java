package com.example.student.doneate.ngo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.student.doneate.R;
import com.example.student.doneate.helper.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CurrentRequestsAdapter extends RecyclerView.Adapter<CurrentRequestsAdapter.MyViewHolder>
    implements View.OnClickListener{

    JSONParser jsonParser = new JSONParser();
    private static String url_mark_donated = "http://10.112.68.50/doneate/mark_donated.php";
    private static String serverMarkDonated = "https://doneate.000webhostapp.com/mark_donated.php";;

    private static String serverDeleteFromReq = "https://doneate.000webhostapp.com/delete_from_requests.php";;
    private static String url_delete_from_req = "http://10.112.68.50/doneate/delete_from_requests.php";

    private Context mContext;
    private List<CurrentRequests> mData;
    private Bitmap receivedBitmap,imgBitmap;
    String photo_url, resID;
    View view;
    public CurrentRequestsAdapter(Context mContext, List<CurrentRequests> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardview_item_current_req, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tvQty.setText(String.valueOf(mData.get(position).getQty()) + " kg");
        holder.tvResName.setText(mData.get(position).getResName());
        holder.tvItemName.setText(mData.get(position).getItemName());
        holder.tvReqTime.setText(mData.get(position).getReqTime());
        holder.tvReqId.setText(mData.get(position).getReqId());
        holder.imgBtnAction.setTag(position + "," + String.valueOf(holder.tvReqId.getText()));
        holder.imgBtnAction.setOnClickListener(this);
        if(mData.get(position).getResApprovalStatus() == 1){
            holder.imgResApproval.setImageResource(R.drawable.green);
        }else{
            holder.imgResApproval.setImageResource(R.drawable.red);
        }
        if(mData.get(position).getQcApprovalStatus() == 1) {
            holder.imgQcApproval.setImageResource(R.drawable.green);
        }else{
            holder.imgQcApproval.setImageResource(R.drawable.red);
        }
        resID = mData.get(position).getImgRes();
//        resID = resID.substring(resID.indexOf("/"));
        new GetImage(resID, holder.imgRes).execute();
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public void onClick(View view) {
        String[] tagArray = String.valueOf(view.getTag()).split(",");
        int position = (Integer.parseInt(tagArray[0]));
        String reqId = tagArray[1];
        showPopupMenu(view,position,reqId);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tvReqId, tvQty, tvResName, tvItemName, tvReqTime;
        ImageView imgResApproval, imgQcApproval, imgRes, imgBtnAction;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvReqId = itemView.findViewById(R.id.tvReqId);
            tvResName = itemView.findViewById(R.id.tvResName);
            tvItemName = itemView.findViewById(R.id.itemName);
            tvReqTime = itemView.findViewById(R.id.tvReqAt);
            tvQty = itemView.findViewById(R.id.tvReqQty);
            imgRes = itemView.findViewById(R.id.resImg);
            imgQcApproval = itemView.findViewById(R.id.QcApproval);
            imgResApproval = itemView.findViewById(R.id.resApproval);
            imgBtnAction = itemView.findViewById(R.id.imgBtnAction);
        }
    }

    private class GetImage extends AsyncTask<Void, Void, Bitmap> {
        String name;
        ImageView imageView;
        public GetImage(String name, ImageView imageView){
            this.name = name;
            this.imageView = imageView;
        }
        @Override
        protected Bitmap doInBackground(Void... voids) {
            String serverGetPhoto = "https://doneate.000webhostapp.com/res_images" + name;
            String getResPhoto = "http://10.112.68.50/doneate/res_images" + name;
            try{
                URLConnection connection = new URL(getResPhoto).openConnection();
                connection.setConnectTimeout(1000 * 30);
                connection.setReadTimeout(1000 * 30);
                receivedBitmap = BitmapFactory.decodeStream((InputStream)connection.getContent(),null,null);
                return receivedBitmap;
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Bitmap bitmap){
            super.onPostExecute(bitmap);
            if(bitmap != null){
                imageView.setImageBitmap(bitmap);
//                imgBitmap = bitmap;
//                receivedBitmap = bitmap;

            }
        }
    }

    private void showPopupMenu(View view, final int position, final String reqId) {
        // inflate menu
        PopupMenu popup = new PopupMenu(view.getContext(),view );
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.popup_menu_curr_req, popup.getMenu());
        if(mData.get(position).getQcApprovalStatus() == 1 && mData.get(position).getResApprovalStatus() == 1){
            popup.getMenu().findItem(R.id.addToCollected).setVisible(true);
        }else{
            popup.getMenu().findItem(R.id.addToCollected).setVisible(false);
        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.addToCollected:
                        new AddToDonated(reqId, mData.get(position).getQty(), position).execute();
                        return true;
                    case R.id.deleteReq:
                        new DeleteFromFoodReq(reqId, position).execute();
//                        Toast.makeText(mContext, "Done " + reqId, Toast.LENGTH_SHORT).show();
                        return true;
                    default:
                }
                return false;
            }
        });
        popup.show();
    }

    private class AddToDonated extends AsyncTask<String, String, String >{
        String reqId;
        String reqQty;
        int position;
        int success;
        public AddToDonated(String reqId, String reqQty, int position){
            this.reqId = reqId;
            this.reqQty = reqQty;
            this.position = position;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String msg = "";
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("req_id", reqId));
            pairs.add(new BasicNameValuePair("check_out_qty", reqQty));
            JSONObject jsonObject = jsonParser.makeHttpRequest(url_mark_donated,"GET",pairs);
            try{
                success = jsonObject.getInt("success");
                if(success == 1){
                    msg = "Marked successfully!";

                }else{
                    msg = jsonObject.getString("message");
                }
            }catch (JSONException j){
                j.printStackTrace();
            }catch (Exception e){
                e.printStackTrace();
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            if(success == 1){
                mData.remove(position);
                CurrentRequestsNGO.rvCurReq.removeViewAt(position);
                CurrentRequestsAdapter.this.notifyItemRemoved(position);
                CurrentRequestsAdapter.this.notifyDataSetChanged();
                new DeleteFromFoodReq(reqId,position).execute();
            }
            super.onPostExecute(s);
            Toast.makeText(mContext, s, Toast.LENGTH_SHORT).show();
        }
    }

    private class DeleteFromFoodReq extends AsyncTask<String, String, String>{

        String reqId;
        int position;

        public DeleteFromFoodReq(String reqId, int position) {
            this.reqId = reqId;
            this.position = position;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String msg = "";
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("req_id",reqId));
            JSONObject jsonObject = new JSONParser().makeHttpRequest(url_delete_from_req, "GET",pairs);
            try{
                int success = jsonObject.getInt("success");
                if(success ==  1){
                    msg = "Deleted Successfully";
                }else{
                    msg = "Error while deleting";
                }
            }catch (JSONException j){
                j.printStackTrace();
            }catch (Exception e){
                e.printStackTrace();
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            mData.remove(position);
            CurrentRequestsNGO.rvCurReq.removeViewAt(position);
            CurrentRequestsAdapter.this.notifyItemRemoved(position);
            CurrentRequestsAdapter.this.notifyDataSetChanged();
            super.onPostExecute(s);
        }
    }
}
