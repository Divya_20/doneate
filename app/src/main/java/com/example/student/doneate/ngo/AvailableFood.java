package com.example.student.doneate.ngo;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.student.doneate.R;
import com.example.student.doneate.helper.GPSTracker;
import com.example.student.doneate.helper.JSONParser;
import com.example.student.doneate.helper.SessionManager;
import com.example.student.doneate.helper.ShowInMapDialog;
import com.example.student.doneate.restaurant.ResHomeActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;


public class AvailableFood extends Fragment {

    View rootview;
    JSONParser jParser = new JSONParser();
    JSONParser parserResName = new JSONParser();
//    ArrayList<HashMap<String, String>> arrListAvlFood;
    ArrayList<HashMap<String, String>> arrListLoc;

    private static String url_avl_food = "http://10.112.68.50/doneate/get_avl_food.php";
    private static String serverAvlFood = "https://doneate.000webhostapp.com/get_avl_food.php";

    private static String url_res_details = "http://10.112.68.50/doneate/get_res_details.php";
    private static String serverResDetails = "https://doneate.000webhostapp.com/get_res_details.php";

    JSONArray jaAvlFood = null;
    String resName,r_id;
    ListView lvAvlFood;
    ImageView imgNothing;
    TextView tvYourReq;
    BottomNavigationView mBottomNavigationView;
    String photo_url;
    AvlFoodAdapter adapter;


    double latitude, longitude;
    ArrayList<Food> foodList = new ArrayList<Food>();

    static final String KEY_NAME = "item_name";
    static final String KEY_QTY = "qty";
    static final String KEY_ID = "f_id";
    static final String KEY_TYPE = "type";
    static final String KEY_RESNAME = "name";
    static final String KEY_RES_ID ="r_id";
    static final String KEY_PREP_TIME = "prep_time";
    static final String KEY_UPLOAD_TIME = "upload_time";
    static final String KEY_PHOTO_URL = "res_image";

    public AvailableFood() {
    }

//    @Override
//    public void onActivityCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);

//
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_available_food, container, false);
        imgNothing = rootview.findViewById(R.id.imgNothing);
        tvYourReq = rootview.findViewById(R.id.food_req);
        tvYourReq.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int textlength = charSequence.length();
                ArrayList<Food> tempArrayList = new ArrayList<Food>();
                for(Food food: foodList){
                    if (textlength == 0) {
                        tempArrayList = foodList;
                    } else {
                        if (food.getQty() >= Double.parseDouble(charSequence.toString())) {
                            tempArrayList.add(food);
                        }
                    }

                }
                AvlFoodAdapter mAdapter = new AvlFoodAdapter(getActivity(), tempArrayList);
                lvAvlFood.setAdapter(mAdapter);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
//        mBottomNavigationView = ((NgoHomeActivity)getActivity()).findViewById(R.id.bottom_nav);
//        mBottomNavigationView.setVisibility(View.VISIBLE);
        mBottomNavigationView = rootview.findViewById(R.id.bottom_nav);
        mBottomNavigationView.getMenu().getItem(0).setCheckable(false);

//        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) mBottomNavigationView.getLayoutParams();
//        layoutParams.setBehavior(new BottomNavigationBehavior());
//        mBottomNavigationView.setSelectedItemId(R.id.bottom_nav);
//        final Menu menu = mBottomNavigationView.getMenu();
//        MenuItem menuItem = menu.getItem(0);
//        menuItem.setChecked(true);
//
        mBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.nav_refresh:
                        menuItem.setCheckable(true);
                        if(foodList != null && arrListLoc != null) {
                            foodList.clear();
                            arrListLoc.clear();
                        }
                        new LoadAllFood().execute();
                        break;
                    case R.id.nav_map_view:
                        if(arrListLoc.size() > 0) {
                            DialogFragment dialogFragment = ShowInMapDialog.newInstance(Objects.requireNonNull(arrListLoc));
                            dialogFragment.show(Objects.requireNonNull(getFragmentManager()), null);
                        }else{
                            Toast.makeText(getContext(), "Nothing to Show", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case R.id.nav_sort:
                        PopupMenu popup = new PopupMenu(getContext(), rootview.findViewById(R.id.nav_sort));
                        MenuInflater inflater = popup.getMenuInflater();
                        inflater.inflate(R.menu.sort_menu, popup.getMenu());
                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem menuItem) {
                                switch (menuItem.getItemId()){
                                    case R.id.sort_recent:
                                        sortAvailableFood("Time");
                                        break;
                                    case R.id.sort_qty:
                                        sortAvailableFood("Qty");
                                        break;
                                    case R.id.sort_rating:
                                        sortAvailableFood("Rating");
                                        break;
                                    case R.id.sort_distance:
                                        getUserLocation();
                                        sortAvailableFood("Distance");
                                        break;
                                }
//                                Toast.makeText(getContext(),"You Clicked:" + menuItem.getTitle(),Toast.LENGTH_LONG).show();
                                return true;
                            }
                        });
                        popup.show();

                }
                return false;
            }

        });
        lvAvlFood = rootview.findViewById(R.id.list_avl_food);
        lvAvlFood.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String fid = ((TextView) view.findViewById(R.id.lv_f_id)).getText()
                        .toString();
                String avl_qty = ((TextView) view.findViewById(R.id.item_qty)).getText()
                        .toString();
                Intent inFoodDetails = new Intent(getContext(),ViewFoodDetails.class);
                inFoodDetails.putExtra("f_id",fid);
                inFoodDetails.putExtra("avl_qty",avl_qty);
                startActivityForResult(inFoodDetails,100);
            }
        });
        lvAvlFood.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int lastVisibleItem = 0;
            private int lastY = 0;
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                int top = 0;
                if(view.getChildAt(0) != null){
                    top = view.getChildAt(0).getTop();
                }

                if(firstVisibleItem > lastVisibleItem){
                    //scroll down
                    mBottomNavigationView.setVisibility(View.GONE);
                }else if(firstVisibleItem < lastVisibleItem){
                    //scroll up
                    mBottomNavigationView.setVisibility(View.VISIBLE);
                }else{
                    if(top < lastY){
                        //scroll down
                        mBottomNavigationView.setVisibility(View.GONE);
                    }else if(top > lastY){
                        //scroll up
                        mBottomNavigationView.setVisibility(View.VISIBLE);
                    }
                }

                lastVisibleItem = firstVisibleItem;
                lastY = top;
            }
        });
//        arrListAvlFood = new ArrayList<HashMap<String, String >>();
        arrListLoc = new ArrayList<HashMap<String, String>>();
        new LoadAllFood().execute();
//        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) mBottomNavigationView.getLayoutParams();
//        layoutParams.setBehavior(new BottomNavigationBehavior());
        return rootview;
    }

    private void getUserLocation() {
        GPSTracker gps = new GPSTracker(getContext());
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
//            Toast.makeText(getContext(), longitude + " " + latitude, Toast.LENGTH_LONG).show();
        } else {
            gps.showSettingsAlert();
        }
    }

    private void sortAvailableFood(String by) {
        switch (by){
            case"Time":
                Collections.sort(foodList, new Comparator<Food>() {
                    @Override
                    public int compare(Food food, Food t1) {
                        Date d1 = food.getPrepTime(); Date d2 = t1.getPrepTime();
                        if( d1.compareTo(d2) < 0) {
                            Log.d("Lesser Time:", food.getPrepTime() + " " + t1.getPrepTime());
                            return 1;
                        }else if(d1.compareTo(d2) > 0) {
                            Log.d("Greater Time:", food.getPrepTime() + " " + t1.getPrepTime());
                            return -1;
                        }else{
                            Log.d("Time:", food.getPrepTime() + " " + t1.getPrepTime());
                            return 0;
                        }
                    }
                });
                break;
            case"Qty":
                Collections.sort(foodList, new Comparator<Food>() {
                    @Override
                    public int compare(Food food, Food t1) {
                        if( food.getQty() > t1.getQty() ) {
                            return -1;
                        }else if(food.getQty() < t1.getQty()) {
                            return 1;
                        }else{
                            return 0;
                        }
                    }
                });
//                Log.e("Sorted List: ", arrListAvlFood.toString());
//                Toast.makeText(getContext(), arrListAvlFood.toString(), Toast.LENGTH_LONG).show();
//                adapter = new AvlFoodAdapter(getActivity(),arrListAvlFood);
//                lvAvlFood.setAdapter(adapter);
                break;
            case"Rating":
                Collections.sort(foodList, new Comparator<Food>() {
                    @Override
                    public int compare(Food food, Food t1) {
                        if( Double.parseDouble(food.getRating()) > Double.parseDouble(t1.getRating()) ) {
                            return -1;
                        }else if( Double.parseDouble(food.getRating()) < Double.parseDouble(t1.getRating()) ) {
                            return 1;
                        }else{
                            return 0;
                        }
                    }
                });
                break;
            case"Distance":
               Collections.sort(foodList, new Comparator<Food>() {
                   @Override
                   public int compare(Food food, Food t1) {
                       double aDist = calcDistance(Double.parseDouble(food.getLat()), Double.parseDouble(food.getLongi()), latitude, longitude);
                       double bDist = calcDistance(Double.parseDouble(t1.getLat()), Double.parseDouble(t1.getLongi()), latitude, longitude);
                       if( aDist > bDist ) {
                           return 1;
                       }else if(aDist < bDist) {
                           return -1;
                       }else{
                           return 0;
                       }
                   }
               });
                break;
        }
        adapter.notifyDataSetChanged();
    }

    private double calcDistance(double fromLat, double fromLon, double toLat, double toLon) {
        double radius = 6378137;   // approximate Earth radius, *in meters*
        double deltaLat = toLat - fromLat;
        double deltaLon = toLon - fromLon;
        double angle = 2 * Math.asin( Math.sqrt(
                Math.pow(Math.sin(deltaLat/2), 2) +
                        Math.cos(fromLat) * Math.cos(toLat) *
                                Math.pow(Math.sin(deltaLon/2), 2) ) );
        return radius * angle;
    }

    //    private class LoadAllFood extends AsyncTask<String, String, String>{
//        ProgressDialog loadingFood;
//        @Override
//        protected void onPreExecute() {
//            mBottomNavigationView.setVisibility(View.GONE);
//            loadingFood = new ProgressDialog(getContext());
//            loadingFood.setMessage("Loading available food...");
//            loadingFood.show();
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            List<NameValuePair> params = new ArrayList<NameValuePair>();
//            String ngoId = new SessionManager(getContext()).getUserId();
//            if(ngoId != null || !ngoId.equals("")) {
//                params.add(new BasicNameValuePair("n_id", ngoId));
//            }
//            JSONObject json = jParser.makeHttpRequest(url_avl_food, "GET", params);
//            Log.d("All Products: ", json.toString());
//            try {
//                int success = json.getInt("success");
//
//                if (success == 1) {
//
//                    jaAvlFood = json.getJSONArray("avl_food");
//
//                    // looping through All Products
//                    for (int i = 0; i < jaAvlFood.length(); i++) {
//                        JSONObject c = jaAvlFood.getJSONObject(i);
//
//                        String f_id = c.getString("f_id");
//                        String item_name = c.getString("item_name");
//                        String type = c.getString("type");
//                        String prep_time = c.getString("prep_time");
//                        String upload_time = c.getString("upload_time");
//                        Double qty = c.getDouble("qty");
//                        resName = c.getString("res_name");
//                        String lat = c.getString("lat");
//                        String longi = c.getString("longi");
//                        String rating = c.getString("g_rating");
//
//                        HashMap<String, String> map = new HashMap<String, String>();
//                        HashMap<String, String> hmLatLong = new HashMap<String, String>();
//
//                        map.put("f_id", f_id);
//                        map.put("item_name", item_name);
//                        map.put("type",type);
//                        map.put("prep_time",prep_time);
//                        map.put("upload_time",upload_time);
//                        map.put("qty",qty.toString());
//                        map.put("r_id",r_id);
//                        map.put("name",resName);
//                        map.put("g_rating", rating);
//                        map.put(KEY_PHOTO_URL,photo_url);
//
//                        hmLatLong.put("lat",lat);
//                        hmLatLong.put("longi",longi);
//
//                        arrListAvlFood.add(map);
//                        arrListLoc.add(hmLatLong);
//                    }
//                } else {
//
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//        @Override
//        protected void onPostExecute(String file_url) {
//            loadingFood.dismiss();
//            if(arrListAvlFood.size() > 0) {
//                adapter = new AvlFoodAdapter(getActivity(), arrListAvlFood);
//                lvAvlFood.setAdapter(adapter);
//                mBottomNavigationView.setVisibility(View.VISIBLE);
//            }else{
//                imgNothing.setVisibility(View.VISIBLE);
//                tvYourReq.setVisibility(View.GONE);
//            }
//        }
//    }
    private class LoadAllFood extends AsyncTask<Food, String, String>{
        ProgressDialog loadingFood;
        @Override
        protected void onPreExecute() {
            mBottomNavigationView.setVisibility(View.GONE);
            loadingFood = new ProgressDialog(getContext());
            loadingFood.setMessage("Loading available food...");
            loadingFood.show();
        }

        @Override
        protected String doInBackground(Food... foods) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            String ngoId = new SessionManager(getContext()).getUserId();
            if(ngoId != null || !ngoId.equals("")) {
                params.add(new BasicNameValuePair("n_id", ngoId));
            }
            JSONObject json = jParser.makeHttpRequest(url_avl_food, "GET", params);
            Log.d("All Food: ", json.toString());
            try {
                int success = json.getInt("success");

                if (success == 1) {

                    jaAvlFood = json.getJSONArray("avl_food");

                    // looping through All Products
                    for (int i = 0; i < jaAvlFood.length(); i++) {
                        JSONObject c = jaAvlFood.getJSONObject(i);

                        String f_id = c.getString("f_id");
                        String item_name = c.getString("item_name");
                        String type = c.getString("type");
                        String prep_time = c.getString("prep_time");
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                        Date prepDateTime = sdf.parse(prep_time);
                        String upload_time = c.getString("upload_time");
                        Double qty = c.getDouble("qty");
                        resName = c.getString("res_name");
                        String r_id = c.getString("r_id");
                        String lat = c.getString("lat");
                        String longi = c.getString("longi");
                        String rating = c.getString("g_rating");

//                        HashMap<String, String> map = new HashMap<String, String>();
//                        HashMap<String, String> hmLatLong = new HashMap<String, String>();
//
//                        map.put("f_id", f_id);
//                        map.put("item_name", item_name);
//                        map.put("type",type);
//                        map.put("prep_time",prep_time);
//                        map.put("upload_time",upload_time);
//                        map.put("qty",qty.toString());
//                        map.put("r_id",r_id);
//                        map.put("name",resName);
//                        map.put("g_rating", rating);
//                        map.put(KEY_PHOTO_URL,photo_url);
//
//                        hmLatLong.put("lat",lat);
//                        hmLatLong.put("longi",longi);
//
//                        arrListAvlFood.add(map);
//                        arrListLoc.add(hmLatLong
                        foodList.add(new Food(f_id, r_id, item_name,type, prepDateTime, upload_time, lat, longi,rating, resName, qty));
                        HashMap<String, String> hmLatLong = new HashMap<String, String>();
                        hmLatLong.put("lat",lat);
                        hmLatLong.put("longi",longi);
                        arrListLoc.add(hmLatLong);
                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String file_url) {
            loadingFood.dismiss();
            if(foodList.size() > 0) {
                adapter = new AvlFoodAdapter(getActivity(), foodList);
                lvAvlFood.setAdapter(adapter);
                mBottomNavigationView.setVisibility(View.VISIBLE);
            }else{
                imgNothing.setVisibility(View.VISIBLE);
                tvYourReq.setVisibility(View.GONE);
            }
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        foodList.clear();
        arrListLoc.clear();
        new LoadAllFood().execute();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onResume() {
        super.onResume();
        if(adapter != null)
            adapter.notifyDataSetChanged();
    }


}
