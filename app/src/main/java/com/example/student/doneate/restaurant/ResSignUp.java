package com.example.student.doneate.restaurant;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.student.doneate.R;
import com.example.student.doneate.helper.CheckUserExists;
import com.example.student.doneate.helper.JSONParser;
import com.example.student.doneate.helper.MD5;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ResSignUp extends AppCompatActivity implements ResStep1Reg.GetResStepOneData, ResStep2Reg.GetResStepTwoData {


    JSONParser jsonParser = new JSONParser();
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    String resId, resName, resMail, resPhn, resAdd1, resAdd2, resPwd, resType, resGrating;
    Double resLat, resLong;
    String encPwd;
    private static String url_create_res = "http://10.112.68.50/doneate/create_restaurant.php";
    private static String serverUrl = "https://doneate.000webhostapp.com/create_restaurant.php";
//    String ServerURL = "jdbc:mysql://10.112.68.50/doneate";
//    private static final String USER = "doneate";
//    private static final String PASS = "doneate";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_res_sign_up);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        viewPager = findViewById(R.id.RsignUp_vpager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
    }

    public void selectIndex(int newIndex) {
        viewPager.setCurrentItem(newIndex);
    }
    @Override
    public void onSetBasicDetails(String name, String email, String phn, double lat, double longi, String addLine1, String addLine2){
        resName = name; resMail = email; resPhn = phn; resAdd1 = addLine1; resAdd2 = addLine2;
        resLat = lat; resLong = longi;


//        Toast.makeText(getApplicationContext(), resName+resMail+resPhn, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSetOtherDetails(String type, String gRating, String pwd){
        resType = type; resGrating = gRating; resPwd = pwd;
        encPwd = MD5.getHash(resPwd);
//        Toast.makeText(getApplicationContext(), resGrating+resPwd, Toast.LENGTH_LONG).show();
    }

    public void registerRestaurant(){
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String name = resName.replaceAll(" ","");
        resId = "R" + name.substring(0,4) + resAdd2.substring(0,3).toUpperCase();
        resId = resId + String.valueOf(resPhn.substring(resPhn.length() - 3)) + ts;


        //        DBInteraction dbInteraction = new DBInteraction();
//        String response = dbInteraction.insertExecute(Qry,resId,resName,resMail,resPhn,resLat.toString(),resLong.toString(),resAdd1,resAdd2,resType,resGrating,encPwd);
//        Toast.makeText(getApplicationContext(), response , Toast.LENGTH_LONG).show();

        Send sendResData = new Send();
        sendResData.execute();

//        Toast.makeText(getApplicationContext(), resId , Toast.LENGTH_LONG).show();
    }
    @Override
    public void onBackPressed() {
        int currentPosition = viewPager.getCurrentItem();
        if (currentPosition != 0) {
            viewPager.setCurrentItem(viewPager.getCurrentItem()-1);
        } else {
            super.onBackPressed();
        }
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter{
        public ViewPagerAdapter(FragmentManager fragmentManager){
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position){
            switch (position){
                case 0: return new ResStep1Reg();
                case 1: return new ResStep2Reg();

            }
            return null;
        }

        @Override
        public int getCount(){
            return 2;
        }
    }

    private class Send extends AsyncTask<String, String, String> {
        String msg = "";
        ProgressDialog registering;

        @Override
        protected void onPreExecute(){

            registering = new ProgressDialog(ResSignUp.this);
            registering.setMessage("Registering...");
            registering.setIndeterminate(false);
            registering.setCancelable(true);
            registering.show();
        }
        @Override
        protected String doInBackground(String... params){
            List<NameValuePair> resData = new ArrayList<NameValuePair>();
            resData.add(new BasicNameValuePair("r_id",resId));
            resData.add(new BasicNameValuePair("name",resName));
            resData.add(new BasicNameValuePair("uname",resMail));
            resData.add(new BasicNameValuePair("phn_no",resPhn));
            resData.add(new BasicNameValuePair("lat",resLat.toString()));
            resData.add(new BasicNameValuePair("longi",resLong.toString()));
            resData.add(new BasicNameValuePair("add_line1",resAdd1));
            resData.add(new BasicNameValuePair("add_line2",resAdd2));
            resData.add(new BasicNameValuePair("category",resType));
            resData.add(new BasicNameValuePair("g_rating",resGrating));
            resData.add(new BasicNameValuePair("pwd",encPwd));
            resData.add(new BasicNameValuePair("dec_pwd",resPwd));
            JSONObject json = jsonParser.makeHttpRequest(url_create_res,"POST", resData);
//            Log.d("Create Response", json.toString());
            try {
                int success = json.getInt("success");

                if (success == 1) {
                    msg = "Registered Successfully";
//                    Intent i = new Intent(getApplicationContext(), ThanksActivity.class);
//                    startActivity(i);


                } else {
                    // failed to create product
                    msg = json.getString("error");
                }
            } catch (JSONException e) {
                msg = e.getMessage().toString();
                e.printStackTrace();
            }

            return msg;

        }

        @Override
        protected void onPostExecute(String msg){
            registering.dismiss();
            Toast.makeText(ResSignUp.this, msg, Toast.LENGTH_LONG).show();
            Intent ResHome = new Intent(getApplicationContext(), ResHomeActivity.class);
            ResHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(ResHome);
//            Toast.makeText(getApplicationContext(),encPwd,Toast.LENGTH_LONG).show();
        }
    }


}
