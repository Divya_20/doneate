package com.example.student.doneate.qc.ui.history;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.student.doneate.R;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

public class QcHistoryAdapter extends RecyclerView.Adapter<QcHistoryAdapter.MyViewHolder> {

    private Context context;
    private List<QcHistory> mData;
    private Bitmap receivedBitmap;
    String resId;
    public QcHistoryAdapter(Context context, List<QcHistory> mData) {
        this.context = context;
        this.mData = mData;
    }

    @NonNull
    @Override
    public QcHistoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.card_view_qc_history, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QcHistoryAdapter.MyViewHolder myViewHolder, int i) {
        myViewHolder.tvQty.setText(String.valueOf(mData.get(i).getCheckOutQty()) + " kg");
        myViewHolder.tvResName.setText(mData.get(i).getResName());
        myViewHolder.tvItemName.setText(mData.get(i).getItemName());
        myViewHolder.tvCheckOut.setText(mData.get(i).getCheckOutTime());
        myViewHolder.tvReqId.setText(mData.get(i).getDonId());
        resId = mData.get(i).getImgRes();
        resId = resId.substring(resId.indexOf("/"));
        new GetImage(resId, myViewHolder.imgRes).execute();
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tvReqId, tvQty, tvResName, tvItemName, tvCheckOut;
        ImageView imgRes;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvReqId = itemView.findViewById(R.id.tvReqId);
            tvResName = itemView.findViewById(R.id.tvResName);
            tvItemName = itemView.findViewById(R.id.itemName);
            tvCheckOut = itemView.findViewById(R.id.tvCheckOut);
            tvQty = itemView.findViewById(R.id.tvReqQty);
            imgRes = itemView.findViewById(R.id.resImg);

        }
    }

    private class GetImage extends AsyncTask<Void, Void, Bitmap> {
        String name;
        ImageView imageView;
        public GetImage(String name, ImageView imageView){
            this.name = name;
            this.imageView = imageView;
        }
        @Override
        protected Bitmap doInBackground(Void... voids) {
            String serverGetPhoto = "https://doneate.000webhostapp.com/res_images" + name;
            String getResPhoto = "http://10.112.68.50/doneate/res_images" + name;
            try{
                URLConnection connection = new URL(getResPhoto).openConnection();
                connection.setConnectTimeout(1000 * 30);
                connection.setReadTimeout(1000 * 30);
                receivedBitmap = BitmapFactory.decodeStream((InputStream)connection.getContent(),null,null);
                return receivedBitmap;
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Bitmap bitmap){
            super.onPostExecute(bitmap);
            if(bitmap != null){
                imageView.setImageBitmap(bitmap);
//                imgBitmap = bitmap;
//                receivedBitmap = bitmap;

            }
        }
    }

}
