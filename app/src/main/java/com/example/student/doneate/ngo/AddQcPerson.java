package com.example.student.doneate.ngo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.student.doneate.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import customfonts.MyEditText;


public class AddQcPerson extends Fragment {

    View rootview;
    MyEditText edName, edUname, edPhone, edPwd, edQual, edCnfPwd;
    EditText edOTP;
    RadioGroup rgGender;
    RadioButton rbSelectedGender;
    Button btnAddQc, btnConfirm;
    String name, uname, pwd, qual, cnfPwd, gender, phone, receivedOTP, codeSent;
    private GetQcPersonData qcPersonData;
    AlertDialog alert;
    FirebaseAuth firebaseAuth;
    public AddQcPerson() {
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        firebaseAuth = FirebaseAuth.getInstance();
        rootview = inflater.inflate(R.layout.fragment_add_qc_person, container, false);
        edName = rootview.findViewById(R.id.qc_name);
        edUname = rootview.findViewById(R.id.qc_uname);
        edPhone = rootview.findViewById(R.id.qc_phone);
        edQual = rootview.findViewById(R.id.qc_qualification);
//        edPwd = rootview.findViewById(R.id.qc_pwd);
//        edCnfPwd = rootview.findViewById(R.id.qc_cnf_pwd);
        rgGender = rootview.findViewById(R.id.rdgroup_qc_gender);
        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                rbSelectedGender = rootview.findViewById(i);
                gender = rbSelectedGender.getText().toString();
            }
        });
        btnAddQc = rootview.findViewById(R.id.btn_add_qc);
        btnAddQc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = edName.getText().toString().trim();
                uname = edUname.getText().toString().trim();
                phone = edPhone.getText().toString().trim();
                qual = edQual.getText().toString().trim();
                sendVerificationCode();
//                pwd = edPwd.getText().toString().trim();
//                cnfPwd = edCnfPwd.getText().toString().trim();
//                qcPersonData.onGetQcPersonData(name, uname, phone, pwd, qual, gender);
//                ((NgoHomeActivity)getActivity()).registerQc();
            }
        });
        return rootview;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            qcPersonData = (GetQcPersonData)context;
        }catch (ClassCastException ce){
            ce.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        qcPersonData = null;
    }

    public interface GetQcPersonData{
        void onGetQcPersonData(String name, String uname, String phone, String pass, String qual, String gender);
    }

    private void sendVerificationCode(){
        ViewGroup viewGroup = rootview.findViewById(R.id.ngo_reg_frame1);
        View dialogView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_verify, viewGroup,false);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(dialogView);

        alert = builder.create();
        alert.show();
        edOTP = dialogView.findViewById(R.id.editTextOtp);

        btnConfirm = dialogView.findViewById(R.id.btnConfirmOTP);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                receivedOTP = edOTP.getText().toString().trim();
                verifySignInCode();
            }
        });
        PhoneAuthProvider.getInstance().verifyPhoneNumber(phone,60,TimeUnit.SECONDS,getActivity(),mCallbacks);
    }

    private void verifySignInCode(){
        try {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(codeSent, receivedOTP);
            signInWithPhoneAuthCredential(credential);
        }catch (Exception e){
            Toast.makeText(getContext(), "Wrong OTP please try again", Toast.LENGTH_SHORT).show();
            alert.dismiss();
            sendVerificationCode();
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            alert.dismiss();
                            qcPersonData.onGetQcPersonData(name, uname, phone, receivedOTP, qual, gender);
                            ((NgoHomeActivity)getActivity()).registerQc();
                            FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
                            tx.replace(R.id.frames_ngo, new ShowAllQC());
                            tx.commit();
                        }else{
                            Toast.makeText(getContext(), "Wrong OTP please try again", Toast.LENGTH_SHORT).show();
                            alert.dismiss();
                            sendVerificationCode();
                        }
                    }
                });
    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(getContext(), e.getMessage().toString(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            Toast.makeText(getContext(), "Code Sent:" + s, Toast.LENGTH_LONG).show();
            codeSent = s;
        }
    };
}
