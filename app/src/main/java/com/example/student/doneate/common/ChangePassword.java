package com.example.student.doneate.common;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.student.doneate.R;
import com.example.student.doneate.helper.JSONParser;
import com.example.student.doneate.helper.MD5;
import com.example.student.doneate.helper.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import customfonts.MyEditText;
import customfonts.MyTextView;

public class ChangePassword extends AppCompatActivity {

    MyEditText edNewPwd, edNewCnfPwd;
    MyTextView edUname;
    Button btnChangePwd;
    String uname, utype;

    private static String url_change_pwd = "http://10.112.68.50/doneate/change_password.php";
    private static String serverUrlChangePwd = "https://doneate.000webhostapp.com/change_password.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        Intent i = getIntent();
        uname = i.getStringExtra("uname");
        utype = i.getStringExtra("utype");

        edUname = findViewById(R.id.edUname);
        edNewPwd = findViewById(R.id.edNewPwd);
        edNewCnfPwd = findViewById(R.id.edCnfNewPwd);
        edUname.setText(uname);

        btnChangePwd = findViewById(R.id.btnChngPwd);
        btnChangePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newPwd = edNewPwd.getText().toString().trim();
                String newCnfPwd = edNewCnfPwd.getText().toString().trim();

                if(newPwd.equals(newCnfPwd)){
                    new SendChangePwd(newPwd, utype).execute();
                }else{
                    Toast.makeText(getApplicationContext(),"Password and Confirm Password does not match", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private class SendChangePwd extends AsyncTask<String, String, String>{
        String new_pwd, utype;

        public SendChangePwd(String new_pwd, String utype) {
            this.new_pwd = new_pwd;
            this.utype = utype;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String encPwd = MD5.getHash(new_pwd);
            String msg;
            JSONParser jsonParser = new JSONParser();
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("uname", uname));
            pairs.add(new BasicNameValuePair("new_pwd",encPwd));
            pairs.add(new BasicNameValuePair("utype", utype));
            JSONObject jsonObject = jsonParser.makeHttpRequest(url_change_pwd, "POST", pairs);
            try{
                int success = jsonObject.getInt("success");
                if(success == 1){
                    msg = "Password changed successfully";
                }else{
                    msg = jsonObject.getString("message");
                }
            }catch (JSONException j){
                msg = "Error while changing password";
                j.printStackTrace();

            }catch (Exception e){
                msg = "Poor internet connection";
                e.printStackTrace();
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            if(s.equals("Password changed successfully")){
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
                Intent inBackToSignIn = new Intent(ChangePassword.this, SignInActivity.class);
                startActivity(inBackToSignIn);
            }
            super.onPostExecute(s);
        }
    }
}
