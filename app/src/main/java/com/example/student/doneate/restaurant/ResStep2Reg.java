package com.example.student.doneate.restaurant;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.student.doneate.R;

import java.util.regex.Pattern;

import customfonts.MyEditText;

public class ResStep2Reg extends Fragment {

    Button btnFinish;
    ImageView back_btn;
    RadioGroup typeOfRes;
    RadioButton rdSelectedType;
    MyEditText edRPwd, edRCnfPwd, edRGrating;
    String rPwd, rCnfPwd, rGrating,rType;

    ImageView chkLower,chkUpper, chkSpecial, chkDigit,chkLength;

    public interface GetResStepTwoData {
        void onSetOtherDetails(String type, String g_rating, String pwd);
    }
    GetResStepTwoData getResStepTwoData;
    public ResStep2Reg() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootview = inflater.inflate(R.layout.fragment_res_step2_reg, container, false);
        chkLower = rootview.findViewById(R.id.chkLower);
        chkUpper = rootview.findViewById(R.id.chkUpper);
        chkDigit = rootview.findViewById(R.id.chkDigit);
        chkSpecial = rootview.findViewById(R.id.chkSpecial);
        chkLength = rootview.findViewById(R.id.chkLength);

        edRPwd = rootview.findViewById(R.id.res_pass);
        edRPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String password = edRPwd.getText().toString();
                validatePassword(password);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edRCnfPwd = rootview.findViewById(R.id.res_cnf_pass);

        edRGrating = rootview.findViewById(R.id.g_rating);

        typeOfRes = rootview.findViewById(R.id.radioTypeofRes);
        typeOfRes.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                rdSelectedType = rootview.findViewById(checkedId);
                rType = rdSelectedType.getText().toString();
            }
        });
        int selectedId  = typeOfRes.getCheckedRadioButtonId();
        rdSelectedType = rootview.findViewById(selectedId);
//        rType = rdSelectedType.getText().toString();
        btnFinish = rootview.findViewById(R.id.btn_RsignUp);
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ResSignUp)getActivity()).selectIndex(2);
                rPwd = edRPwd.getText().toString().trim();
                rCnfPwd = edRCnfPwd.getText().toString().trim();
                rGrating = edRGrating.getText().toString().trim();
                if(checkValidEntry(rGrating, rPwd, rCnfPwd)) {
                    getResStepTwoData.onSetOtherDetails(rType, rGrating, rPwd);
                    ((ResSignUp) getActivity()).registerRestaurant();
                }
//                Intent itThanks = new Intent(getContext(), ThanksActivity.class);
//                startActivity(itThanks);
            }
        });
        back_btn = rootview.findViewById(R.id.bckinF2);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ResSignUp)getActivity()).onBackPressed();
            }
        });
        return rootview;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            getResStepTwoData = (GetResStepTwoData)context;
        }catch (ClassCastException ce){
            ce.printStackTrace();
        }
    }

    public boolean checkValidEntry(String rGrating, String rPwd, String rCnfPwd){
        boolean isValid = true;
        if(rGrating.equals("")){
            edRGrating.setError("Please fill this out!");
            isValid = false;
        }
        if(rPwd.equals("")){
            edRPwd.setError("Please fill this out!");
            isValid = false;
        }
        if(rCnfPwd.equals("")){
            edRCnfPwd.setError("Please fill this out!");
            isValid = false;
        }else{
            if(!rCnfPwd.equals(rPwd)){
                edRCnfPwd.setError("Password and Confirm password must be same!");
                isValid = false;
            }
        }
        return isValid;
    }

    public void validatePassword(String password){
        Pattern uppperCase = Pattern.compile("[A-Z]");
        Pattern lowerCase = Pattern.compile("[a-z]");
        Pattern digitCase = Pattern.compile("[0-9]");
        Pattern specialChar = Pattern.compile("[^A-Za-z0-9]");

        if(!lowerCase.matcher(password).find()){
            chkLower.setVisibility(View.GONE);
        }else{
            chkLower.setVisibility(View.VISIBLE);
        }

        if(!uppperCase.matcher(password).find()){
            chkUpper.setVisibility(View.GONE);
        }else{
            chkUpper.setVisibility(View.VISIBLE);
        }

        if(!digitCase.matcher(password).find()){
            chkDigit.setVisibility(View.GONE);
        }else{
            chkDigit.setVisibility(View.VISIBLE);
        }

        if(!specialChar.matcher(password).find()){
            chkSpecial.setVisibility(View.GONE);
        }else{
            chkSpecial.setVisibility(View.VISIBLE);
        }

        if(password.length() < 8 || password.length() > 20){
            chkLength.setVisibility(View.GONE);
        }else{
            chkLength.setVisibility(View.VISIBLE);
        }
    }
}
