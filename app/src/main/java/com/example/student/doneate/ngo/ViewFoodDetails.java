package com.example.student.doneate.ngo;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.student.doneate.R;
import com.example.student.doneate.helper.JSONParser;
import com.example.student.doneate.helper.MapDialogFragment;
import com.example.student.doneate.helper.SessionManager;
import com.example.student.doneate.restaurant.ReqFoodAdapter;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ViewFoodDetails extends AppCompatActivity {

    JSONParser jParser = new JSONParser();
    private static String url_get_food_details = "http://10.112.68.50/doneate/get_food_details.php";
    private static String serverGetFoodDetails = "https://doneate.000webhostapp.com/get_food_details.php";

    private static final String url_create_food_request = "http://10.112.68.50/doneate/create_food_request.php";
    private static final String serverCreateFoodReq = "https://doneate.000webhostapp.com/create_food_request.php";

    private static final String url_update_food_request = "http://10.112.68.50/doneate/update_food_request.php";
    private static final String serverUpdateFoodReq = "https://doneate.000webhostapp.com/update_food_request.php";

    private static final String url_delete_food_request = "http://10.112.68.50/doneate/delete_food_request.php";
    private static final String serverDeleteFoodReq = "https://doneate.000webhostapp.com/delete_food_request.php";


    TextView tvName, tvQty, tvUpload, tvPrep, tvResName, tvAddress, tvReqQty, tvLblReq;
    ImageView imgType, profilePic, imgShowInMap, imgBack;
    EditText edReqQty;
    String name, qty, upload, prep, resName, address, type, photoUrl, resID;
    Double rating;
    String f_id, n_id, req_id, reqQty, avlQty;
    Button btnReqThis, btnCancel, btnReqQty;
    private static final int REQUEST_CODE = 101;
    Bitmap receivedBitmap;

    //    Intent i;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_food_details);

        Intent intent = getIntent();
        f_id = intent.getStringExtra("f_id");
        avlQty = intent.getStringExtra("avl_qty");
        avlQty = avlQty.substring(0, avlQty.indexOf(" "));
        edReqQty = findViewById(R.id.edReqQty);
        tvReqQty = findViewById(R.id.txt_req_qty);
        tvLblReq = findViewById(R.id.lbl_req_qty);
        tvName = findViewById(R.id.food_name);
        tvQty = findViewById(R.id.food_qty);
        tvResName = findViewById(R.id.food_res_name);
        tvAddress = findViewById(R.id.food_res_add);
        tvUpload = findViewById(R.id.txt_food_upload);
        tvPrep = findViewById(R.id.txt_food_prep);
        profilePic = findViewById(R.id.profile_pic);

        imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        imgShowInMap = findViewById(R.id.img_map);
//        imgShowInMap.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new MapDialogFragment().show(getSupportFragmentManager(),null);
//            }
//        });
        new GetData().execute();

        btnReqThis = findViewById(R.id.btn_request_this);
        btnReqThis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnReqQty = findViewById(R.id.btn_req_ok);
                edReqQty.setVisibility(View.VISIBLE);
                edReqQty.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        Double qty;
                        try {
                            qty = Double.parseDouble(edReqQty.getText().toString().trim());
                            if (qty > Double.parseDouble(avlQty)) {
                                edReqQty.setError("Not available!!");
                            } else {
                                reqQty = qty.toString();
                            }
                        } catch (NumberFormatException n) {
                            edReqQty.setError("Invalid Quantity");
                        }


                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
                btnReqQty.setVisibility(View.VISIBLE);
                btnReqQty.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        createFoodRequest();

                    }
                });
                btnReqThis.setVisibility(View.GONE);


            }
        });
        btnCancel = findViewById(R.id.btn_cancel_request);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnReqThis.setVisibility(View.VISIBLE);
                btnCancel.setVisibility(View.GONE);
                tvLblReq.setVisibility(View.GONE);
                tvReqQty.setVisibility(View.GONE);
                edReqQty.setVisibility(View.GONE);
                btnReqQty.setVisibility(View.GONE);
                new CancelFoodRequest().execute();
            }
        });
    }


    private class GetData extends AsyncTask<String, String, String> {
        String msg;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                int success;
                List<NameValuePair> args = new ArrayList<NameValuePair>();
                args.add(new BasicNameValuePair("f_id", f_id));

                JSONObject json = jParser.makeHttpRequest(
                        url_get_food_details, "GET", args);
                Log.d("Food Details", json.toString());
                success = json.getInt("success");
                if (success == 1) {
                    msg = "Food Details";
                    JSONArray foodObj = json.getJSONArray("food");
                    JSONObject food = foodObj.getJSONObject(0);
                    name = food.getString("item_name");
                    qty = food.getString("qty");
                    resName = food.getString("res_name");
                    address = food.getString("add_line1");
                    type = food.getString("type");
                    rating = food.getDouble("rating");
                    upload = food.getString("upload_time");
                    prep = food.getString("prep_time");
                    photoUrl = food.getString("photo_url");
                } else {
                    msg = "No Food found!";
                }
            } catch (JSONException j) {
                msg = j.getMessage().toString();
            } catch (Exception e) {
                msg = "Could not connect to the server";
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (msg != null) {
                tvName.setText(name);
                tvQty.setText(qty + " kg");
                tvUpload.setText(upload);
                tvPrep.setText(prep);
                tvResName.setText(resName);
                tvAddress.setText(address);
                new GetImage(photoUrl).execute();
            }
        }
    }

    private class GetImage extends AsyncTask<Void, Void, Bitmap> {
        String name;

        public GetImage(String name) {
            this.name = name;
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            resID = photoUrl.substring(photoUrl.indexOf("/") + 1);
            String serverGetPhoto = "https://doneate.000webhostapp.com/res_images/" + resID;
            photoUrl = "http://10.112.68.50/doneate/res_images/" + resID;
            try {
                URLConnection connection = new URL(photoUrl).openConnection();
                connection.setConnectTimeout(1000 * 30);
                connection.setReadTimeout(1000 * 30);
                receivedBitmap = BitmapFactory.decodeStream((InputStream) connection.getContent(), null, null);
                return receivedBitmap;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (bitmap != null) {
                profilePic.setImageBitmap(receivedBitmap);
            }
        }
    }

    public void createFoodRequest() {
//        Timestamp ts = new Timestamp(System.currentTimeMillis());
//        Long tsLong = System.currentTimeMillis()/1000;
        SessionManager sessionManager = new SessionManager(getApplicationContext());
        String ts = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        n_id = sessionManager.getUserId();
        if (n_id != null && !n_id.equals("")) {
            if (sessionManager.isQcAdded()) {
                req_id = n_id.substring(0, 5) + f_id.substring(0, 4) + ts;
                new CreateFoodRequest().execute();
            } else {
                Toast.makeText(getApplicationContext(), "Cannot proceed without adding a QC!!", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please Login to Continue", Toast.LENGTH_LONG).show();
        }
    }

    private class CreateFoodRequest extends AsyncTask<String, String, String> {
        String msg = "";
        int success;
        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("n_id", n_id));
            pairs.add(new BasicNameValuePair("f_id", f_id));
            pairs.add(new BasicNameValuePair("req_id", req_id));
            pairs.add(new BasicNameValuePair("req_qty", reqQty));

            JSONObject json = jParser.makeHttpRequest(url_create_food_request, "GET", pairs);
//            Log.d("Create Response", json.toString());
            try {
                success = json.getInt("success");

                if (success == 1) {
                    msg = json.getString("message");
                } else {
                    msg = json.getString("error");
                }
            } catch (JSONException e) {
                msg = e.getMessage();
                e.printStackTrace();
            } catch (Exception e) {
                msg = "Could not connect to the server";
            }

            return msg;
        }

        @Override
        protected void onPostExecute(String msg) {
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            edReqQty.setVisibility(View.GONE);
            btnReqQty.setVisibility(View.GONE);
            tvReqQty.setVisibility(View.VISIBLE);
            tvReqQty.setText(reqQty);
            tvLblReq.setVisibility(View.VISIBLE);

            btnReqThis.setVisibility(View.GONE);
            btnCancel.setVisibility(View.VISIBLE);

            if(success == 1){
                double newAvlQty = Double.parseDouble(avlQty) - Double.parseDouble(reqQty);
                tvQty.setText(String.valueOf(newAvlQty)+ " kg");
            }
            super.onPostExecute(msg);
        }
    }

    private class CancelFoodRequest extends AsyncTask<String, String, String> {
        String msg;

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("req_id", req_id));
            pairs.add(new BasicNameValuePair("req_qty", reqQty));
            pairs.add(new BasicNameValuePair("f_id", f_id));
            JSONObject json = jParser.makeHttpRequest(url_delete_food_request, "POST", pairs);
            try {
                int success = json.getInt("success");

                if (success == 1) {
                    msg = json.getString("message");
                } else {
                    msg = json.getString("error");
                }
            } catch (JSONException e) {
                msg = e.getMessage().toString();
                e.printStackTrace();
            } catch (Exception e) {
                msg = "Could not connect to the server";
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
        }
    }

//    private void sendUpdatedData(){
//
//    }

    @Override
    public void onBackPressed() {
//        sendUpdatedData();
        super.onBackPressed();

    }
}
