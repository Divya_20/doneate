package com.example.student.doneate.ngo;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ResultReceiver;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.student.doneate.R;
import com.example.student.doneate.helper.CheckUserExists;
import com.example.student.doneate.helper.FetchAddressIntentService;
import com.example.student.doneate.common.ChooseTypeActivity;
import com.example.student.doneate.helper.GMailSender;
import com.example.student.doneate.helper.JSONParser;
import com.example.student.doneate.helper.MapDialogFragment;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import customfonts.MyEditText;


public class NGOStep1Reg extends Fragment {

    public interface GetNGOStepOneData {
        void onNGOSetStepOneDetails(String name, String email, String phn, double lat, double longi, String addLine1, String addLine2, String company);
    }
    public interface OnBackPressedListener {
        void onBackPressed();
    }
    public GetNGOStepOneData getNGOStepOneData;
    public OnBackPressedListener onBackPressedListener;
    View rootview;

    Button btnNext, btnConfirm;
    ImageView back_btn;

    MyEditText edNAddLine, edNName, edNEmail, edNPhone, edNCompany;
    EditText edOTP;
    String nName, nEmail, nPhone, nAddLine1, nAddLine2, nCompany;
    String sentOTP, receivedOTP;
    double lat, longi;
    boolean isValidMail;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 2;
//    public FusedLocationProviderClient fusedLocationClient;
    public FusedLocationProviderClient fusedLocationClient;
    private LocationAddressResultReceiver addressResultReceiver;
    private Location currentLocation;
    private LocationCallback locationCallback;

    private static String url_check_user = "http://10.112.68.50/doneate/check_user.php";
    private static String serverUrl = "https://doneate.000webhostapp.com/check_user.php";

    JSONParser jsonParser = new JSONParser();
    boolean isValidUser;

    public static final int DIALOG_FRAGMENT = 1;
    public static final int RESULT_OK = 101;
    public NGOStep1Reg() {
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        addressResultReceiver = new LocationAddressResultReceiver(new Handler());
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        locationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                currentLocation = locationResult.getLocations().get(0);
                getAddress();
            };
        };
        startLocationUpdates();

        rootview = inflater.inflate(R.layout.fragment_ngo_step1_reg, container, false);

        edNName = rootview.findViewById(R.id.ngo_name);
        edNEmail = rootview.findViewById(R.id.ngo_email);
        edNPhone = rootview.findViewById(R.id.ngo_phone_no);
        edNAddLine = rootview.findViewById(R.id.ngo_add_line1);

        edNAddLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fusedLocationClient.removeLocationUpdates(locationCallback);
                DialogFragment dialogFragment = MapDialogFragment.newInstance(1);
                dialogFragment.setTargetFragment(NGOStep1Reg.this,DIALOG_FRAGMENT);
                dialogFragment.show(Objects.requireNonNull(getFragmentManager()),null);
//                new MapDialogFragment().show(getFragmentManager(),null);
            }
        });
        edNCompany = rootview.findViewById(R.id.ngo_company);

        back_btn = rootview.findViewById(R.id.bckinN1);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent itCtypeAct = new Intent(getContext(),ChooseTypeActivity.class);
                startActivity(itCtypeAct);
            }
        });
        btnNext = rootview.findViewById(R.id.N_btn_next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                ((NGOSignUp)getActivity()).selectIndex(1);
                nName = edNName.getText().toString().trim();
                nEmail = edNEmail.getText().toString().trim();
                nPhone = edNPhone.getText().toString().trim();
                nAddLine1 = edNAddLine.getText().toString().trim();

                nCompany = edNCompany.getText().toString().trim();
                if(checkValidEntry(nName, nEmail, nPhone, nAddLine1,nCompany)){
                    new NGOStep1Reg.CheckUserExists().execute();
//                    CheckUserExists checkUserExists = new CheckUserExists(nEmail);
//                    checkUserExists.execute();
//                    if(checkUserExists.getUserExists()){
//                        Toast.makeText(getContext(),"Already Registered", Toast.LENGTH_LONG).show();
//                    }else {
//                        new SendEmail().execute();
//                    }

                    int idx = nAddLine1.lastIndexOf(",");
                    String lastComma = String.valueOf(nAddLine1.charAt(idx));
                    nAddLine1 = nAddLine1.replace(lastComma,".");
                    getNGOStepOneData.onNGOSetStepOneDetails(nName,nEmail,nPhone, lat,longi,nAddLine1,nAddLine2,nCompany);

                }

            }
        });
        return rootview;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            getNGOStepOneData = (NGOStep1Reg.GetNGOStepOneData)context;
            onBackPressedListener = (NGOStep1Reg.OnBackPressedListener)context;
        }catch (ClassCastException ce){
            ce.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getNGOStepOneData = null;
        onBackPressedListener = null;
    }

    private class LocationAddressResultReceiver extends ResultReceiver {
        LocationAddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if(resultCode == 0){
                Log.e("Address","Location null retrying");
                getAddress();
            }

            if(resultCode == 1){
                Toast.makeText(getContext(),"Address not found",Toast.LENGTH_SHORT).show();
            }
            String currentAdd = resultData.getString("address_result");
            showResults(currentAdd);
        }
    }

    private void getAddress() {
        if(!Geocoder.isPresent()){
            Toast.makeText(getContext(), "Can't find current address",Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent(getActivity(),FetchAddressIntentService.class);
        intent.putExtra("add_receiver", addressResultReceiver);
        intent.putExtra("add_location", currentLocation);
        getActivity().startService(intent);
    }

    private void showResults(String currentAdd) {
        String[] address  = currentAdd.split("\n");
//        Toast.makeText(getContext(), String.valueOf(address.length), Toast.LENGTH_LONG).show();
        if(!currentAdd.equals("No address found!")) {
            lat = Double.parseDouble(address[0]);
            longi = Double.parseDouble(address[1]);

            edNAddLine.setText(address[2]);
            nAddLine2 = address[3] + "," + address[4] + "," + address[5];
        }
    }

    private void startLocationUpdates(){
        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
        }else{
            LocationRequest locationRequest = new LocationRequest();
            locationRequest.setInterval(2000);
            locationRequest.setFastestInterval(1000);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
        }
    }

    public void checkEmailCode(){
        ViewGroup viewGroup = rootview.findViewById(R.id.ngo_reg_frame1);
        View dialogView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_verify, viewGroup,false);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(dialogView);

        final AlertDialog alert = builder.create();
        alert.show();
        edOTP = dialogView.findViewById(R.id.editTextOtp);

        btnConfirm = dialogView.findViewById(R.id.btnConfirmOTP);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                receivedOTP = edOTP.getText().toString().trim();

                final ProgressDialog authenticating = ProgressDialog.show(getContext(), "Authenticating", "Please wait while we check the entered code", false,false);
                authenticating.show();
                Handler sender = new Handler(Looper.getMainLooper());
                {
                    sender.post(new Runnable() {
                        @Override
                        public void run() {
                            if (receivedOTP.equals(sentOTP)) {
                                isValidMail = true;
                                authenticating.dismiss();
                                alert.dismiss();
                                if(checkValidEntry(nName, nEmail, nPhone, nAddLine1,nCompany)) {
                                    getNGOStepOneData.onNGOSetStepOneDetails(nName, nEmail, nPhone, lat, longi, nAddLine1, nAddLine2, nCompany);
                                    ((NGOSignUp) getActivity()).selectIndex(1);
                                }

                            } else {
                                isValidMail = false;
                                Toast.makeText(getContext(), "Wrong OTP Please Try Again", Toast.LENGTH_LONG).show();
                                try {
                                    authenticating.dismiss();
                                    alert.dismiss();
                                    //Asking user to enter otp again
                                    checkEmailCode();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                }
            }
        });
    }

    public boolean checkValidEntry(String nName, String nEmail, String nPhone, String nAddLine1, String nCompany){
        boolean isValid = true;
        if(nName.equals("")){
            edNName.setError("Please fill this out!");
            isValid = false;
        }
        if(nEmail.equals("")){
            edNEmail.setError("Please fill this out!");
            isValid = false;
        }else{
            if (!(Patterns.EMAIL_ADDRESS.matcher(nEmail).matches())) {
                edNEmail.setError("Invalid Email Address!");
                isValid = false;
            }
        }
        if(nPhone.equals("")){
            edNPhone.setError("Please fill this out!");
            isValid = false;
        }else{
            if(nPhone.length()>10){
                edNPhone.setError("Invalid Mobile number");
                isValid = false;
            }
            String phoneNoPattern = "((\\+*)((0[ -]+)*|(91 )*)(\\d{12}+|\\d{10}+))|\\d{5}([- ]*)\\d{6}";
            if(!nPhone.matches(phoneNoPattern)){
                edNPhone.setError("Invalid Mobile number");
                isValid = false;
            }
        }
        if(nAddLine1.equals("")){
            edNAddLine.setError("Please fill this out!");
            isValid = false;
        }
        if(nCompany.equals("")){
            edNCompany.setError("Please fill this out!");
        }
        return isValid;
    }

    private class CheckUserExists extends AsyncTask<String, String, String> {
        String res;
        ProgressDialog verifying;
        @Override
        protected void onPreExecute(){
            verifying = new ProgressDialog(getContext());
            verifying.setMessage("Hold on...");
            verifying.show();
        }
        @Override
        protected String doInBackground(String... params){
            try{
                int success;
                List<NameValuePair> args = new ArrayList<NameValuePair>();
                args.add(new BasicNameValuePair("u_name",nEmail));
                JSONObject json = jsonParser.makeHttpRequest(
                        url_check_user, "GET", args);
                Log.d("User Details", json.toString());
                success = json.getInt("success");
                if(success == 1){
                    JSONArray userObj = json.getJSONArray("user");
                    JSONObject user = userObj.getJSONObject(0);
                    res = "User Already Exists";
                    isValidUser = true;
                }else{
                    res = "Email does not exist";
                    isValidUser = false;
                }
            }catch(JSONException j){
                res = j.getMessage().toString();
            }
//            try{
////                DBInteraction dbInteraction = new DBInteraction();
////                Connection conn =  dbInteraction.getConn();
//                Class.forName("com.mysql.jdbc.Driver");
//                Connection conn = DriverManager.getConnection(ServerURL,USER,PASS);
//                if(conn == null){
//                    msg = "Connection goes null";
//                }else {
//                    String Qry = "SELECT * FROM user_role WHERE u_name = ? and u_pwd = ? ";
//                    encPwd = MD5.getHash(password);
//                    PreparedStatement selQuery = conn.prepareStatement(Qry);
//                    selQuery.setString(1,username);
//                    selQuery.setString(2,encPwd);
//                    ResultSet rs = selQuery.executeQuery();
//                    if(!rs.isBeforeFirst()){
//                        res = "Invalid Login Credentials";
//                    }else {
//                        userSession = new SessionManager(getApplicationContext());
//                        while (rs.next()) {
//                            uid = rs.getString("u_id");
//                            auth_status = rs.getInt("u_status");
//                            userType = rs.getString("u_type");
////                            res = uid + " " + auth_status;
////                            isValidUser = true;
//                        }
//                        if(auth_status == 1) {
//                            userSession.setUserId(uid);
//                            userSession.setUsername(username);
//                            userSession.setUserType(userType);
//                            isValidUser = true;
//                        }else{
//                            res = "Your credentials are not yet verified";
//                            isValidUser = false;
//                        }
//                    }
//                    conn.close();
//                }
//            }catch (Exception e){
//                msg = "Connection goes wrong";
//                msg = e.toString();
//                e.printStackTrace();
//            }
            return res;
        }

        @Override
        protected void onPostExecute(String msg){
            verifying.dismiss();
            if(isValidUser) {
                Toast.makeText(getContext(), "Already registered", Toast.LENGTH_LONG).show();
            }else{
                new SendEmail().execute();
            }
        }
    }

    private class SendEmail extends AsyncTask<String, String, String> {
        ProgressDialog loading;
        @Override
        protected void onPreExecute(){
            loading = ProgressDialog.show(getActivity(), "Sending Mail", "Please wait...", false, false);
            loading.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String msg = "";
            try {
                GMailSender sender = new GMailSender("doneate.app@gmail.com", "done@ate");
                if(sender.sendMail("OTP from DoneAte",
                        "This is your code to register your email \n" + generateOTP(),
                        "doneate.app@gmail.com", nEmail)) {
                    msg = "Email Sent";
                }
            } catch (Exception e) {
                msg = e.getMessage();
            }
            return msg;
        }
        @Override
        protected void onPostExecute(String msg){
            loading.dismiss();
            if(msg.equals("Email Sent")){
                checkEmailCode();
            }else{
                Toast.makeText(getContext(),"Error sending mail, Verify your email address",Toast.LENGTH_LONG).show();
            }
        }
    }

    public String generateOTP(){
        sentOTP = "";
        String numbers = "0123456789";
        Random rndm_method = new Random();
        for (int i = 0; i < 6; i++){
            sentOTP = sentOTP + numbers.charAt(rndm_method.nextInt(numbers.length()));
        }
        return sentOTP;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case DIALOG_FRAGMENT:
                if(resultCode == RESULT_OK){
                    String Address = data.getStringExtra("key_add");
                    nAddLine2 = data.getStringExtra("add_line2");
                    lat = data.getDoubleExtra("latitude",0.0);
                    longi = data.getDoubleExtra("longitude",0.0);
                    edNAddLine.setText(Address);
                }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        fusedLocationClient.removeLocationUpdates(locationCallback);
    }
}
