package com.example.student.doneate.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SessionManager {

    private SharedPreferences prefs;
    SharedPreferences.Editor editor;
    String username,uid,uType;
    boolean isQcAdded;



    public SessionManager(Context context) {
        // TODO Auto-generated constructor stub
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        editor = prefs.edit();
    }

    public void setUsername(String username) {
        editor.putString("username", username).commit();
    }

    public String getUsername() {
        username = prefs.getString("username","");
        return username;
    }
    public void setUserId(String userId){
        editor.putString("userid", userId).commit();
    }
    public String getUserId(){
        uid = prefs.getString("userid","");
        return uid;
    }

    public void setUserType(String userType){
        editor.putString("userType",userType).commit();
    }
    public String getUserType(){
        uType = prefs.getString("userType","");
        return uType;
    }

    public boolean isQcAdded() {
        isQcAdded = prefs.getBoolean("isQcAdded",isQcAdded);
        return isQcAdded;
    }

    public void setQcAdded(boolean qcAdded) {
        editor.putBoolean("isQcAdded",qcAdded).commit();
    }

    public void clearAll(){
        editor.remove("username");
        editor.remove("userid");
        editor.remove("userType");
        editor.remove("isQcAdded");
        editor.commit();
        username = null;
        uid = null;
        uType = null;
    }
}
