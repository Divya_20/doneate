package com.example.student.doneate.restaurant;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.student.doneate.R;
import com.example.student.doneate.helper.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ReqFoodAdapter extends BaseAdapter implements CompoundButton.OnCheckedChangeListener {
    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private ArrayList<String> checkedList = new ArrayList<String>();
    private static LayoutInflater inflater=null;
    Bitmap image,receivedBitmap;

    Date uploadDateTime,prepDateTime;

    private static String url_updt_food = "http://10.112.68.50/doneate/update_food_req.php";
    private static String serverUpdtFood = "https://doneate.000webhostapp.com/update_food_req.php";
    JSONParser jParser = new JSONParser();


    public ReqFoodAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        resetCheckedList();
    }

    public ArrayList<String> getCheckedList(){
        return checkedList;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        HashMap<String, String> food = new HashMap<String, String>();

        food = data.get(position);
        return food.get(FoodRequests.KEY_ID);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public ReqFoodAdapter() {
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)  {

        View vi = convertView;
        String prepTime = "", uploadTime = "";
        if(convertView == null)
            vi = inflater.inflate(R.layout.list_row_req_food, null);
        TextView fid = vi.findViewById(R.id.lv_req_id);
        TextView name = vi.findViewById(R.id.item_name);
        TextView qty = vi.findViewById(R.id.item_req_qty);
        TextView ngoAdd = vi.findViewById(R.id.ngo_address);
        TextView resName = vi.findViewById(R.id.ngoName);
        ImageView type = vi.findViewById(R.id.type);
        ImageView thumb_image = vi.findViewById(R.id.ngo_image);
//        ImageView btn_req_confirm = vi.findViewById(R.id.btn_req_confirm);
        CheckBox checkBox = vi.findViewById(R.id.chkThisRequest);
        checkBox.setOnCheckedChangeListener(this);
        HashMap<String, String> food = new HashMap<String, String>();

        food = data.get(position);
//        try {
//            SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//            uploadTime = food.get(FoodRequests.KEY_UPLOAD_TIME);
//            prepTime = food.get(FoodRequests.KEY_PREP_TIME);
//            uploadDateTime = dateParser.parse(uploadTime);
//            prepDateTime = dateParser.parse(prepTime);
//            uploadTime = dateFormatter.format(uploadDateTime).toString();
//            prepTime = dateFormatter.format(prepDateTime).toString();
////            upload_time.setText(dateFormatter.format(uploadDateTime));
////            prep_time.setText(dateFormatter.format(prepDateTime));
////            System.out.println(dateFormatter.format(date));
////            uploadTime = sdf2.format(sdf.parse());
////            prepTime = sdf2.format(sdf.parse(food.get(AvailableFood.KEY_PREP_TIME)));
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }

        checkBox.setTag(position + "," + food.get(FoodRequests.KEY_ID));
        checkBox.setChecked(checkedList.get(position)!="0");
        fid.setText(food.get(FoodRequests.KEY_ID));
        name.setText(food.get(FoodRequests.KEY_NAME));
        qty.setText(food.get(FoodRequests.KEY_REQ_QTY) + " kg");
        ngoAdd.setText(food.get(FoodRequests.KEY_NGO_ADD));
//        upload_time.setText(uploadTime);
//        prep_time.setText(prepTime);
        resName.setText(food.get(FoodRequests.KEY_NGO_NAME));
        if(food.get(FoodRequests.KEY_TYPE).equals("Veg"))
            type.setImageResource(R.drawable.veg);
        else if(food.get(FoodRequests.KEY_TYPE).equals("Non-Veg"))
            type.setImageResource(R.drawable.non_veg);
        String photo_url =  "ngo_images/IMG" + food.get(FoodRequests.KEY_NGO_ID) + ".JPG" ;
        GetImage getImage = new GetImage(thumb_image,photo_url);
        getImage.execute();

//        btn_req_confirm.setTag(food.get(FoodRequests.KEY_ID));
//        btn_req_confirm.setTag(position + "," + food.get(FoodRequests.KEY_ID));
//        btn_req_confirm.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                String tag = (String) view.getTag();
//
//                String arrayTag[] = ((String) view.getTag()).split(",");
//                String key = arrayTag[1];
//                new UpdateRequest(key).execute();
//                int pos = Integer.parseInt(arrayTag[0]);
////                Toast.makeText(activity,String.valueOf(pos),Toast.LENGTH_SHORT).show();
////                Object toRemove = ReqFoodAdapter.this.getItem(Integer.parseInt(pos));
//                data.remove(pos);
//                ReqFoodAdapter.this.notifyDataSetChanged();
//
//
//            }
//        });
        return vi;
    }

    private class GetImage extends AsyncTask<Void, Void, Bitmap> {
        ImageView imageView;
        String name;
        public GetImage(ImageView imageView, String name){
            this.imageView = imageView;
            this.name = name;
        }
        @Override
        protected Bitmap doInBackground(Void... voids) {

            try{

                URLConnection connection = new URL(name).openConnection();
                connection.setConnectTimeout(1000 * 30);
                connection.setReadTimeout(1000 * 30);
                receivedBitmap = BitmapFactory.decodeStream((InputStream)connection.getContent(),null,null);
                receivedBitmap = Bitmap.createScaledBitmap(receivedBitmap,100,100,false);
                return receivedBitmap;
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Bitmap bitmap){
            super.onPostExecute(bitmap);
            if(bitmap != null){
                imageView.setImageBitmap(receivedBitmap);
            }
        }
    }

    private class UpdateRequest extends AsyncTask<String, String, String>{
        String reqId;

        public UpdateRequest(String reqId){
            this.reqId = reqId;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String msg = "";
            List<NameValuePair> pairs = new ArrayList<>();
            pairs.add(new BasicNameValuePair("req_id", reqId));
            JSONObject jsonObject = jParser.makeHttpRequest(url_updt_food,"POST",pairs);
            Log.d("Request Status:",jsonObject.toString());

            try{
                int success = jsonObject.getInt("success");
                if(success == 1){
                     msg = "Request Confirmed!";
                }else{
                    msg = "Error processing request";
                }
            }catch (JSONException j){
                j.printStackTrace();
                msg = "Could not update request";
            }catch (Exception e){
                msg = "Poor internet connection";
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Toast.makeText(activity, s, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

        String arrayTag[] = ((String) compoundButton.getTag()).split(",");
        String key = arrayTag[1];
        int pos = Integer.parseInt(arrayTag[0]);
//        compoundButton.setChecked(checkedList.get(pos)!="0");
        if(b) {
            checkedList.set(pos, key);
//            Toast.makeText(activity,"Checked " + key + " " + pos,Toast.LENGTH_SHORT).show();
        }else{
            checkedList.set(pos,"0");
//            Toast.makeText(activity,"Unchecked " + key + " " + pos, Toast.LENGTH_SHORT).show();
        }
    }

    public void removeItem(int o){
        if(o < data.size())
            data.remove(o);
        ReqFoodAdapter.this.notifyDataSetChanged();
    }

    public void updateSingleRequest(String reqId, int position){
        new UpdateRequest(reqId).execute();
        data.remove(position);
        ReqFoodAdapter.this.notifyDataSetChanged();

    }
    public void resetCheckedList(){
        checkedList.clear();
        for(int i = 0; i < data.size(); i++ ){
            checkedList.add(i,"0");
        }
    }

    public void clearAllData(){
        data.clear();
        ReqFoodAdapter.this.notifyDataSetChanged();
    }
}
