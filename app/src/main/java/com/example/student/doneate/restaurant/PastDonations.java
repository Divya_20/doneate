package com.example.student.doneate.restaurant;

public class PastDonations {
    private String donId;
    private String ngoName;
    private String qcName;
    private String checkOutTime;
    private String checkOutQty;
    private String imgNgo;
    private String itemName;

    public PastDonations() {
    }

    public PastDonations(String donId, String ngoName, String itemName, String qcName, String checkOutTime, String checkOutQty, String imgNgo) {
        this.donId = donId;
        this.ngoName = ngoName;
        this.qcName = qcName;
        this.checkOutTime = checkOutTime;
        this.checkOutQty = checkOutQty;
        this.imgNgo = imgNgo;
        this.itemName = itemName;
    }

    public String getDonId() {
        return donId;
    }

    public String getNgoName() {
        return ngoName;
    }

    public String getQcName() {
        return qcName;
    }

    public String getCheckOutTime() {
        return checkOutTime;
    }

    public String getCheckOutQty() {
        return checkOutQty;
    }

    public String getImgNgo() {
        return imgNgo;
    }

    public String getItemName() {
        return itemName;
    }
}
