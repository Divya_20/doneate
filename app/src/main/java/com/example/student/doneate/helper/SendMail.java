package com.example.student.doneate.helper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.student.doneate.restaurant.ResStep1Reg;

public class SendMail extends AsyncTask<String, String, String> {
    ProgressDialog loading;
    Activity activity;
    String mail;

    public SendMail(Activity activity, String mail){
        this.activity = activity;
        this.mail = mail;
    }
    @Override
    protected void onPreExecute(){
        loading = ProgressDialog.show(activity, "Sending Mail", "Please wait...", false, false);
        loading.show();
    }

    @Override
    protected String doInBackground(String... params) {
        String msg = "";
        try {
            GMailSender sender = new GMailSender("doneate.app@gmail.com", "done@ate");
            if(sender.sendMail("OTP from DoneAte",
                    "This is your code to register your email \n",
                    "doneate.app@gmail.com", mail)) {
                msg = "Email Sent";
            }
        } catch (Exception e) {
            msg = e.getMessage();
        }
        return msg;
    }
    @Override
    protected void onPostExecute(String msg){
        loading.dismiss();
        if(msg.equals("Email Sent")){
            ;
        }else{
            Toast.makeText(activity,"Error sending mail, Verify your email address",Toast.LENGTH_LONG).show();
        }
    }
}