package com.example.student.doneate.qc;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.student.doneate.R;
import com.example.student.doneate.qc.ui.history.FragmentHistory;
import com.example.student.doneate.qc.ui.home.FragmentHomeQc;
import com.example.student.doneate.qc.ui.profile.FragmentProfileQc;

import java.util.List;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class QcDefaultActivity extends AppCompatActivity{

    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qc_default);
        BottomNavigationView navView = findViewById(R.id.nav_view);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("DoneAte");

        navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                selectItemNavigation(menuItem);
                return false;
            }
        });
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, new FragmentHomeQc());
        transaction.commit();
    }

    private void selectItemNavigation(MenuItem menuItem) {
        int id = menuItem.getItemId();
        Fragment fragment = null;
        Class FragmentClass;
        switch (id){
            case R.id.nav_qc_home:
                FragmentClass = FragmentHomeQc.class;
                break;
            case R.id.nav_qc_history:
                FragmentClass = FragmentHistory.class;
                break;
            case R.id.nav_qc_profile:
                FragmentClass = FragmentProfileQc.class;
                break;
            default:
                FragmentClass = FragmentHomeQc.class;

        }
        try{
            fragment = (Fragment)FragmentClass.newInstance();
        }catch (Exception e){
            e.printStackTrace();
        }
        if(fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.nav_host_fragment, fragment).commit();
            menuItem.setChecked(true);
//            toolbar.setTitle(menuItem.getTitle());
        }
    }

    @Override
    public void onBackPressed() {
        finish();
//        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
//        if (fragmentList != null) {
//            finish();
//            for(Fragment fragment : fragmentList){
//                if(fragment instanceof FragmentProfileQc.OnBackPressedListener){
//                    ((FragmentProfileQc.OnBackPressedListener)fragment).onBackPressed();
//                }
//            }
//        }
    }

    //    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.bottom_menu, menu);
//        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
//        tx.replace(R.id.nav_host_fragment, new FragmentHomeQc());
//        tx.commit();
//        return true;
//    }
}
