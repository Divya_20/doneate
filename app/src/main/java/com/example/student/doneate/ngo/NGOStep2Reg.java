package com.example.student.doneate.ngo;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.student.doneate.R;

import java.util.Calendar;
import java.util.regex.Pattern;

import customfonts.MyEditText;


public class NGOStep2Reg extends Fragment {

    public interface GetNGOStepTwoData {
        void onNGOSetStepTwoDetails(String lic_no, String lic_pur_date, String pwd);
    }
    public GetNGOStepTwoData getNGOStepTwoData;
    View rootview;

    Button btnFinish;
    ImageView back_btn;
    ImageView chkLower,chkUpper, chkSpecial, chkDigit,chkLength;
    MyEditText edNPwd, edNCnfPwd, edNLicNo;
    TextView tvLicPurDate;

    final Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener dateSetListener;

    String nPwd, nCnfPwd, nLicNo,nLicPurDate;
    public NGOStep2Reg() {

    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_ngo_step2_reg, container, false);
        chkLower = rootview.findViewById(R.id.chkLower);
        chkUpper = rootview.findViewById(R.id.chkUpper);
        chkDigit = rootview.findViewById(R.id.chkDigit);
        chkSpecial = rootview.findViewById(R.id.chkSpecial);
        chkLength = rootview.findViewById(R.id.chkLength);
        edNPwd = rootview.findViewById(R.id.ngo_pass);
        edNPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String password = edNPwd.getText().toString();
                validatePassword(password);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edNLicNo = rootview.findViewById(R.id.ngo_licence_no);
        edNCnfPwd = rootview.findViewById(R.id.ngo_cnf_pass);
        tvLicPurDate = rootview.findViewById(R.id.ngo_lic_date);
        tvLicPurDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int year = myCalendar.get(Calendar.YEAR);
                int month = myCalendar.get(Calendar.MONTH);
                int day = myCalendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(
                        getContext(), android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        dateSetListener,year,month,day);
                dialog.getDatePicker().setMaxDate(myCalendar.getTimeInMillis());
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month += 1;
                tvLicPurDate.setText(dayOfMonth + "/" + month + "/" + year);
            }
        };

        back_btn = rootview.findViewById(R.id.bckinN2);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NGOSignUp)getActivity()).onBackPressed();
            }
        });

        btnFinish = rootview.findViewById(R.id.N_btn_sign_up);
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NGOSignUp)getActivity()).selectIndex(2);
                nPwd = edNPwd.getText().toString().trim();
                nCnfPwd = edNCnfPwd.getText().toString().trim();
                nLicNo = edNLicNo.getText().toString().trim();
                nLicPurDate = tvLicPurDate.getText().toString().trim();
                if(checkValidEntry(nLicPurDate, nLicNo, nPwd, nCnfPwd)) {
                    getNGOStepTwoData.onNGOSetStepTwoDetails(nLicNo, nLicPurDate, nPwd);
                    ((NGOSignUp) getActivity()).registerNGO();
                }

            }
        });
        return rootview;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            getNGOStepTwoData = (NGOStep2Reg.GetNGOStepTwoData)context;
        }catch (ClassCastException ce){
            ce.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getNGOStepTwoData = null;
    }
    public boolean checkValidEntry(String nLicPurDate, String nLicNo, String nPwd, String nCnfPwd){
        boolean isValid = true;
        if(nLicPurDate.equals("")){
            tvLicPurDate.setError("Please fill this out!");
            isValid = false;
        }
        if(nPwd.equals("")){
            edNPwd.setError("Please fill this out!");
            isValid = false;
        }
        if(nCnfPwd.equals("")){
            edNCnfPwd.setError("Please fill this out!");
            isValid = false;
        }else{
            if(!nCnfPwd.equals(nPwd)){
                edNCnfPwd.setError("Password and Confirm password must be same!");
                isValid = false;
            }
        }
        return isValid;
    }

    public void validatePassword(String password){
        Pattern uppperCase = Pattern.compile("[A-Z]");
        Pattern lowerCase = Pattern.compile("[a-z]");
        Pattern digitCase = Pattern.compile("[0-9]");
        Pattern specialChar = Pattern.compile("[^A-Za-z0-9]");

        if(!lowerCase.matcher(password).find()){
            chkLower.setVisibility(View.GONE);
        }else{
            chkLower.setVisibility(View.VISIBLE);
        }

        if(!uppperCase.matcher(password).find()){
            chkUpper.setVisibility(View.GONE);
        }else{
            chkUpper.setVisibility(View.VISIBLE);
        }

        if(!digitCase.matcher(password).find()){
            chkDigit.setVisibility(View.GONE);
        }else{
            chkDigit.setVisibility(View.VISIBLE);
        }

        if(!specialChar.matcher(password).find()){
            chkSpecial.setVisibility(View.GONE);
        }else{
            chkSpecial.setVisibility(View.VISIBLE);
        }

        if(password.length() < 8 || password.length() > 20){
            chkLength.setVisibility(View.GONE);
        }else{
            chkLength.setVisibility(View.VISIBLE);
        }
    }

}
