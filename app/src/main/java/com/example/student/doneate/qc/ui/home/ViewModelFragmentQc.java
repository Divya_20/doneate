package com.example.student.doneate.qc.ui.home;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class ViewModelFragmentQc extends ViewModel {

    private MutableLiveData<String> mText;

    public ViewModelFragmentQc() {
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}