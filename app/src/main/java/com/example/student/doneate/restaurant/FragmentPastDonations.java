package com.example.student.doneate.restaurant;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.student.doneate.R;
import com.example.student.doneate.helper.JSONParser;
import com.example.student.doneate.helper.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class FragmentPastDonations extends Fragment {
    View rootview;
    JSONParser jParser = new JSONParser();
    private static String url_past_donations = "http://10.112.68.50/doneate/get_past_res_donations.php";
    private static String serverPastDonations = "https://doneate.000webhostapp.com/get_past_res_donations.php";

    int noOfReq;
    JSONArray jaCurRequests = null;
    List<PastDonations> pastDonationsList = new ArrayList<PastDonations>();;
    PastDonationsAdapter adapter;
    String resName, resId;
    Double qty;
    RecyclerView rvPastDonations;
    ImageView imgNothing;
    ProgressBar progressBar;
    public FragmentPastDonations() {
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_past_donations, container, false);
        rvPastDonations = rootview.findViewById(R.id.rvPastDonations);
        progressBar = rootview.findViewById(R.id.loading);
        imgNothing = rootview.findViewById(R.id.imgNothing);
        resId = new SessionManager(getContext()).getUserId();

        if(resId != null && !resId.equals("")){
            new LoadPastDonations().execute();
        }
        return rootview;
    }



    private class LoadPastDonations extends AsyncTask<String, String, String >{

        String msg;
        int noOfDonations;
        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("r_id", resId));
            JSONObject jsonObject = jParser.makeHttpRequest(url_past_donations, "GET", pairs);
            Log.d("Past Donations", jsonObject.toString());

            try {
                int success = jsonObject.getInt("success");
                if(success == 1) {
                    JSONArray jaPastDonations = jsonObject.getJSONArray("donations");
                    if(jaPastDonations.length() > 0){
                        noOfDonations = jaPastDonations.length();
                        for(int i = 0; i < jaPastDonations.length(); i++){
                            JSONObject object = jaPastDonations.getJSONObject(i);
                            String d_id = object.getString("d_id");
                            String qty = object.getString("check_out_qty");
                            String item_name = object.getString("item_name");
                            String ngoName = object.getString("ngo_name");
                            String ngoImgUrl = object.getString("photo_url");
                            String checkOutTime = object.getString("check_out_time");
                            String qcName = object.getString("qc_name");
                            pastDonationsList.add(new PastDonations(d_id, ngoName, item_name, qcName, checkOutTime, qty, ngoImgUrl));
                        }
                    }else {
                        noOfDonations = 0;
                        msg = jsonObject.getString("message");
                        return msg;
                    }
                }else{
                    msg = jsonObject.getString("error");
                }
            }catch (JSONException j){
                j.printStackTrace();
            }catch (Exception e){
                msg = "Poor internet connection";
                e.printStackTrace();
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBar.setVisibility(View.GONE);
            if(noOfDonations > 0 ){
                adapter = new PastDonationsAdapter(getContext(), pastDonationsList);
                rvPastDonations.setLayoutManager(new GridLayoutManager(getContext(), 2));
                rvPastDonations.setAdapter(adapter);
            }else {
                imgNothing.setVisibility(View.VISIBLE);
            }
            super.onPostExecute(s);
        }
    }
}
