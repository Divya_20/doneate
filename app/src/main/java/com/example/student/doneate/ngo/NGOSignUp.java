package com.example.student.doneate.ngo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.student.doneate.R;
import com.example.student.doneate.helper.JSONParser;
import com.example.student.doneate.helper.MD5;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NGOSignUp extends AppCompatActivity implements NGOStep1Reg.GetNGOStepOneData, NGOStep2Reg.GetNGOStepTwoData {

    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;

    String ngoId,ngoName, ngoEmail, ngoPhn, ngoAddLine1, ngoAddLine2, ngoCompany,ngoLic_no, ngoLic_pur_date, ngoPwd;
    double ngoLat, ngoLongi;
    String encPwd, dateString;
    Date date;
    JSONParser jsonParser = new JSONParser();

    String url_create_ngo = "http://10.112.68.50/doneate/create_ngo.php";
    String serverUrl = "https://doneate.000webhostapp.com/create_ngo.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ngo_sign_up);

        viewPager = findViewById(R.id.NsignUp_vpager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);

    }
    @Override
    public void onNGOSetStepOneDetails(String name, String email, String phn, double lat, double longi, String addLine1, String addLine2, String company){
        ngoName = name; ngoEmail = email; ngoPhn = phn; ngoLat = lat; ngoLongi = longi;
        ngoAddLine1 = addLine1; ngoAddLine2 = addLine2; ngoCompany = company;
    }

    @Override
    public void onNGOSetStepTwoDetails(String lic_no, String lic_pur_date, String pwd){
        ngoLic_no = lic_no; ngoPwd = pwd; ngoLic_pur_date = lic_pur_date;
//        String myFormat = "yyyy/MM/dd";
    }

    public void registerNGO(){
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String name  = ngoName.replaceAll(" ","");
        ngoId = "N" + name.substring(0,4) + ngoAddLine2.substring(0,3).toUpperCase();
        ngoId = ngoId + String.valueOf(ngoLic_no.substring(ngoLic_no.length() - 3)) + ts;

        try {
//            String startDateString = "12/08/2019";
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
            ngoLic_pur_date = sdf2.format(sdf.parse(ngoLic_pur_date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //        DBInteraction dbInteraction = new DBInteraction();
//        String response = dbInteraction.insertExecute(Qry,resId,resName,resMail,resPhn,resLat.toString(),resLong.toString(),resAdd1,resAdd2,resType,resGrating,encPwd);
//        Toast.makeText(getApplicationContext(), response , Toast.LENGTH_LONG).show();
        Send sendNGOData = new Send();
        sendNGOData.execute();
    }
    public void onBackPressed() {
        int currentPosition = viewPager.getCurrentItem();
        if (currentPosition != 0) {
            viewPager.setCurrentItem(viewPager.getCurrentItem()-1);
        } else {
            super.onBackPressed();
        }
    }

    public void selectIndex(int newIndex) {
        viewPager.setCurrentItem(newIndex);
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        public ViewPagerAdapter(FragmentManager fragmentManager){
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position){
            switch (position){
                case 0: return new NGOStep1Reg();
                case 1: return new NGOStep2Reg();

            }
            return null;
        }

        @Override
        public int getCount(){
            return 2;
        }
    }

    private class Send extends AsyncTask<String, String, String> {
        String msg = "";
        ProgressDialog registering;

        @Override
        protected void onPreExecute(){

            registering = new ProgressDialog(NGOSignUp.this);
            registering.setMessage("Registering...");
            registering.setIndeterminate(false);
            registering.setCancelable(true);
            registering.show();
        }
        @Override
        protected String doInBackground(String... params){
            encPwd = MD5.getHash(ngoPwd);
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("n_id",ngoId));
            pairs.add(new BasicNameValuePair("name",ngoName));
            pairs.add(new BasicNameValuePair("uname",ngoEmail));
            pairs.add(new BasicNameValuePair("phn_no",ngoPhn));
            pairs.add(new BasicNameValuePair("lat",String.valueOf(ngoLat)));
            pairs.add(new BasicNameValuePair("longi",String.valueOf(ngoLongi)));
            pairs.add(new BasicNameValuePair("add_line1",ngoAddLine1));
            pairs.add(new BasicNameValuePair("add_line2",ngoAddLine2));
            pairs.add(new BasicNameValuePair("lic_no",ngoLic_no));
            pairs.add(new BasicNameValuePair("company",ngoCompany));
            pairs.add(new BasicNameValuePair("lic_pur_date",ngoLic_pur_date));
            pairs.add(new BasicNameValuePair("pwd",encPwd));
            pairs.add(new BasicNameValuePair("dec_pwd",ngoPwd));

            JSONObject json = jsonParser.makeHttpRequest(url_create_ngo,"POST", pairs);
//            Log.d("Create Response", json.toString());
            try {
                int success = json.getInt("success");

                if (success == 1) {
                    msg = "Registered Successfully";
//                    Intent i = new Intent(getApplicationContext(), ThanksActivity.class);
//                    startActivity(i);

                } else {
                    msg = json.getString("error");
                }
            } catch (JSONException e) {
                msg = e.getMessage().toString();
                e.printStackTrace();
            }

            return msg;

//            try{
////                DBInteraction dbInteraction = new DBInteraction();
////                Connection conn =  dbInteraction.getConn();
//                Class.forName("com.mysql.jdbc.Driver");
//                Connection conn = DriverManager.getConnection(ServerURL,USER,PASS);
//                if(conn == null){
//                    msg = "Connection goes null";
//                }else {
//                    String Qry = "INSERT INTO restaurant(r_id,name,uname,phn_no,lat,longi,add_line1," +
//                            "add_line2,category,g_rating,pwd) " +
//                            "VALUES(?,?,?,?,?,?,?,?,?,?,?)";
//                    encPwd = MD5.getHash(resPwd);
//                    PreparedStatement insQuery = conn.prepareStatement(Qry);
//                    insQuery.setString(1,resId);
//                    insQuery.setString(2,resName);
//                    insQuery.setString(3,resMail);
//                    insQuery.setString(4,resPhn);
//                    insQuery.setDouble(5,resLat);
//                    insQuery.setDouble(6,resLong);
//                    insQuery.setString(7,resAdd1);
//                    insQuery.setString(8,resAdd2);
//                    insQuery.setString(9,resType);
//                    insQuery.setString(10,resGrating);
//                    insQuery.setString(11,encPwd);
//
//                    int i = insQuery.executeUpdate();
//                    if(i > 0){
//                        msg = "Registered Successfully";
//                        String insUserQry = "INSERT INTO user_role(u_id,u_name,u_pwd,u_type)" +
//                                         "VALUES (?,?,?,?)";
//                        PreparedStatement psinsUser = conn.prepareStatement(insUserQry);
//                        psinsUser.setString(1,resId);
//                        psinsUser.setString(2,resMail);
//                        psinsUser.setString(3,encPwd);
//                        psinsUser.setString(4,"Restaurant");
//
//                        int userInserted = psinsUser.executeUpdate();
//                        if(userInserted > 0){
//
//                        }
//                    }
//                    else{
//                        msg = "Please try again";
//                    }
//                    conn.close();
//                }
//            }catch (Exception e){
//                msg = "Connection goes wrong";
//                msg = e.toString();
//                e.printStackTrace();
//            }

        }

        @Override
        protected void onPostExecute(String msg){
            registering.dismiss();
            Toast.makeText(NGOSignUp.this, msg, Toast.LENGTH_LONG).show();
            Intent NgoHome = new Intent(getApplicationContext(), NgoHomeActivity.class);
            NgoHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(NgoHome);
//            Toast.makeText(getApplicationContext(),encPwd,Toast.LENGTH_LONG).show();
        }
    }
}
