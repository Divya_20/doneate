package com.example.student.doneate.qc.ui.history;

public class QcHistory {
    private String donId;
    private String resName;

    private String checkOutTime;
    private String checkOutQty;
    private String imgRes;
    private String itemName;

    public QcHistory() {
    }

    public QcHistory(String donId, String resName, String itemName, String checkOutTime, String checkOutQty, String imgRes) {
        this.donId = donId;
        this.resName = resName;
        this.checkOutTime = checkOutTime;
        this.checkOutQty = checkOutQty;
        this.imgRes = imgRes;
        this.itemName = itemName;
    }

    public String getDonId() {
        return donId;
    }

    public String getResName() {
        return resName;
    }

    public String getCheckOutTime() {
        return checkOutTime;
    }

    public String getCheckOutQty() {
        return checkOutQty;
    }

    public String getImgRes() {
        return imgRes;
    }

    public String getItemName() {
        return itemName;
    }
}
