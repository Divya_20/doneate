package com.example.student.doneate.qc.ui.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import com.example.student.doneate.R;
import com.example.student.doneate.common.SignInActivity;
import com.example.student.doneate.helper.JSONParser;
import com.example.student.doneate.helper.SessionManager;
import com.example.student.doneate.qc.QcDefaultActivity;
import com.example.student.doneate.qc.ui.home.FragmentHomeQc;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FragmentProfileQc extends Fragment {

    private static String url_qc_details = "http://10.112.68.50/doneate/get_qc_details.php";
    private static String serverUrl = "https://doneate.000webhostapp.com/get_qc_details.php";

    JSONParser jsonParser = new JSONParser();

    TextView tvname, tvphn, tvmail, tvqual, tvCntConfirm, tvCntPen,tvGender, tvHiredBy;
    Button btnLogOut, btnChPass, btnLogout, btnCancel;
    String name, phn, mail, qual, cntConfirm, cntPen, gender, hiredBy;
    String userId;
    View root;
    SessionManager userSession;

//    public interface OnBackPressedListener {
//        void onBackPressed();
//    }
//    public OnBackPressedListener onBackPressedListener;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_qc_profile, container, false);
        tvname = root.findViewById(R.id.qcname);
        tvphn = root.findViewById(R.id.tvqcphn);
        tvmail = root.findViewById(R.id.tvqcmail);
        tvGender = root.findViewById(R.id.tvgender);
        tvqual = root.findViewById(R.id.tvqcqual);
        tvHiredBy = root.findViewById(R.id.tvhiredby);
        tvCntConfirm = root.findViewById(R.id.cntConfirmed);
        tvCntPen = root.findViewById(R.id.cntPending);
        btnChPass = root.findViewById(R.id.btnqcChPass);
        btnChPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        btnLogOut = root.findViewById(R.id.btnqcLogout);
        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup();
            }
        });
        new GetData().execute();
        return root;

    }

    private class GetData extends AsyncTask<String, String, String> {
        String msg;
        ProgressDialog loading = new ProgressDialog(getContext());
        @Override
        protected void onPreExecute(){
            loading.setMessage("Loading Profile..");
            loading.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try{
                userSession = new SessionManager(getContext());
                userId = userSession.getUserId();
                int success;
                List<NameValuePair> args = new ArrayList<NameValuePair>();
                args.add(new BasicNameValuePair("qc_id",userId));

                JSONObject json = jsonParser.makeHttpRequest(
                        url_qc_details, "GET", args);
                Log.d("QC Details", json.toString());
                success = json.getInt("success");

                if(success == 1){
                    cntConfirm = String.valueOf(json.getInt("confirmed_req"));
                    cntPen = String.valueOf(json.getInt("pending_req"));
                    JSONArray restaurantObj = json.getJSONArray("qc_person");
                    JSONObject restaurant = restaurantObj.getJSONObject(0);
                    name = restaurant.getString("name");
                    mail = restaurant.getString("uname");
                    phn = restaurant.getString("phone");
                    gender = restaurant.getString("gender");
                    hiredBy = restaurant.getString("ngo_name");
                    qual = restaurant.getString("qualification");

                }else{
                    msg = "No restaurant found!";
                }
            }catch(JSONException j){
                msg = j.getMessage().toString();
            }catch (Exception e){
                msg = "Poor internet connection";
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            loading.dismiss();
//            if(s != null){
            tvname.setText(name);
            tvmail.setText(mail);
            tvGender.setText(gender);
            tvphn.setText(phn);
            tvqual.setText(qual);
            tvHiredBy.setText(hiredBy);
            tvCntConfirm.setText(cntConfirm);
            tvCntPen.setText(cntPen);
//            }
            super.onPostExecute(s);
        }
    }

    private void showPopup() {
        ViewGroup viewGroup = root.findViewById(R.id.container_qc);
        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(getContext()).inflate(R.layout.custom_dialog, viewGroup, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(dialogView);

        final AlertDialog alert = builder.create();
        alert.show();
        btnLogout = dialogView.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionManager clearSessionManager = new SessionManager(getContext());
                clearSessionManager.clearAll();

                Intent loginIntent = new Intent(getActivity(), SignInActivity.class);
                loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loginIntent);
//                finish();
            }
        });
        btnCancel = dialogView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
    }


//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        try {
//            onBackPressedListener = (FragmentProfileQc.OnBackPressedListener)context;
//        }catch (ClassCastException ce){
//            ce.printStackTrace();
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        onBackPressedListener = null;
//    }
}