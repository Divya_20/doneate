package com.example.student.doneate.common;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.student.doneate.R;

public class MainActivity extends AppCompatActivity {

    TextView sign_in;
    LinearLayout circle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        circle = findViewById(R.id.circle);
        sign_in = findViewById(R.id.sign_in);

        circle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent itChooseType = new Intent(MainActivity.this, ChooseTypeActivity.class);
                startActivity(itChooseType);
            }
        });

        sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent itSignIn = new Intent(MainActivity.this, SignInActivity.class);
                startActivity(itSignIn);
            }
        });
    }
}
