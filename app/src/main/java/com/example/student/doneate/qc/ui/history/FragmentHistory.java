package com.example.student.doneate.qc.ui.history;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import com.example.student.doneate.R;
import com.example.student.doneate.helper.JSONParser;
import com.example.student.doneate.helper.SessionManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FragmentHistory extends Fragment {

    View rootView;
    JSONParser jParser = new JSONParser();

    private static String url_qc_history = "http://10.112.68.50/doneate/get_qc_history.php";
    private static String serverQcHistory = "https://doneate.000webhostapp.com/get_qc_history.php";

    List<QcHistory> qcHistoryList = new ArrayList<QcHistory>();
    QcHistoryAdapter qcHistoryAdapter;
    RecyclerView rvQcHistory;
    ImageView imgNothing;
    ProgressBar progressBar;
    String qcId;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_qc_history, container, false);
        rvQcHistory = rootView.findViewById(R.id.rvHistory);
        imgNothing = rootView.findViewById(R.id.imgNothing);
        progressBar = rootView.findViewById(R.id.loading);

        qcId = new SessionManager(getContext()).getUserId();
        if(!qcId.equals("") || qcId != null){
            new LoadQcHistory().execute();
        }
        return rootView;
    }

    private class LoadQcHistory extends AsyncTask<String, String, String>{

        int size;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            String msg = "";
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("qc_id",qcId));
            JSONObject object = jParser.makeHttpRequest(url_qc_history, "GET",pairs);
            Log.d("Qc History", object.toString());

            try {
                int success = object.getInt("success");
                if(success == 1){
                    JSONArray qcHistory = object.getJSONArray("qc_history");
                    if(qcHistory.length() > 0){
                        size = qcHistory.length();
                        for (int i = 0; i < qcHistory.length(); i++){
                            JSONObject historyJSONObject = qcHistory.getJSONObject(i);
                            String d_id = historyJSONObject.getString("d_id");
                            String qty = historyJSONObject.getString("check_out_qty");
                            String item_name = historyJSONObject.getString("item_name");
                            String resName = historyJSONObject.getString("res_name");
                            String resImgUrl = historyJSONObject.getString("photo_url");
                            String checkOutTime = historyJSONObject.getString("check_out_time");

                            qcHistoryList.add(new QcHistory(d_id, resName, item_name, checkOutTime, qty, resImgUrl));

                        }
                    }else{
                        size = 0;
                        return "";
                    }
                }else{
                    msg = object.getString("error");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }catch (Exception ex){
                msg = "Poor internet connection";
                ex.printStackTrace();
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressBar.setVisibility(View.GONE);
            if(size > 0){
                qcHistoryAdapter = new QcHistoryAdapter(getContext(), qcHistoryList);
                rvQcHistory.setLayoutManager(new GridLayoutManager(getContext(), 2));
                rvQcHistory.setAdapter(qcHistoryAdapter);
            }else{
                imgNothing.setVisibility(View.VISIBLE);
            }
        }
    }
}