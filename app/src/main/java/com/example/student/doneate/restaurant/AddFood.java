package com.example.student.doneate.restaurant;

import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.student.doneate.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;


public class AddFood extends Fragment {

    GetUploadFoodData uploadFoodData;
    Button btnUpload;
    RadioGroup typeOfFood;
    RadioButton selectedTypeOfFood;
    TextView tvQty, tvName, tvTime;
    String qty, name, time, type;

    SimpleDateFormat f = new SimpleDateFormat("yyyyMMddHHmmss");
    private int CalendarDate,CalendarMonth, CalendarYear, CalendarHour, CalendarMinute;
    String format, timeStamp;
    Calendar calendar;
    TimePickerDialog timepickerdialog;
    public AddFood() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootview = inflater.inflate(R.layout.fragment_add_food, container, false);

        tvQty = rootview.findViewById(R.id.item_qty);
        tvQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                qty = tvQty.getText().toString().trim();
                if(qty.length() > 0 && qty != null) {
                    if (Double.parseDouble(qty) <= 5) {
                        tvQty.setError("Very Small Quantity");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tvName = rootview.findViewById(R.id.item_name);
        tvTime = rootview.findViewById(R.id.prep_time);
        tvTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar = Calendar.getInstance();
                CalendarDate = calendar.get(Calendar.DATE);
                CalendarMonth = calendar.get(Calendar.MONTH);
                CalendarYear = calendar.get(Calendar.YEAR);
                CalendarHour = calendar.get(Calendar.HOUR_OF_DAY);
                CalendarMinute = calendar.get(Calendar.MINUTE);

                timepickerdialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Calendar temp=Calendar.getInstance();
                        temp.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        temp.set(Calendar.MINUTE,minute);
                        if (hourOfDay == 0) {
                            hourOfDay += 12;
                            format = "AM";
                        }
                        else if (hourOfDay == 12) {
                            format = "PM";
                        }
                        else if (hourOfDay > 12) {
                            hourOfDay -= 12;
                            format = "PM";
                        }
                        else {
                            format = "AM";
                        }
                        String hourFormat = String.format("%02d", hourOfDay);
                        String minFormat = String.format("%02d", minute);

                        if(temp.after(GregorianCalendar.getInstance())){
                            Toast.makeText(getContext(),"Time greater than current time!!", Toast.LENGTH_SHORT).show();
                        }else{
                            calendar.set(CalendarYear, CalendarMonth,CalendarDate,hourOfDay,minute,0);
                            timeStamp = f.format(calendar.getTime());
                            tvTime.setText(hourFormat + ":" + minFormat + " " + format);
                        }

                    }

                },CalendarHour, CalendarMinute,false);
                timepickerdialog.show();
            }
        });
        typeOfFood = rootview.findViewById(R.id.food_type);
        typeOfFood.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                selectedTypeOfFood = rootview.findViewById(checkedId);
                type = selectedTypeOfFood.getText().toString().trim();
            }
        });
        btnUpload = rootview.findViewById(R.id.btn_upload_food);
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                qty = tvQty.getText().toString().trim();
                if(Double.parseDouble(qty) <= 5){
                    tvQty.setError("Very Small Quantity");
                }
                name = tvName.getText().toString().trim();
//                time = tvTime.getText().toString();

                uploadFoodData.onUploadFoodData(qty,type,name,timeStamp);
                ((ResHomeActivity) getActivity()).uploadFood();
            }
        });
        return rootview;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            uploadFoodData = (GetUploadFoodData)context;
        }catch (ClassCastException c){
            c.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }


    public interface GetUploadFoodData {
        // TODO: Update argument type and name
        void onUploadFoodData(String qty, String type, String name, String prep_time);
    }
}
