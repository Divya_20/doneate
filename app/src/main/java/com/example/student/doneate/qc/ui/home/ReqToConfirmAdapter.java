package com.example.student.doneate.qc.ui.home;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.student.doneate.R;
import com.example.student.doneate.helper.JSONParser;
import com.example.student.doneate.helper.SessionManager;
import com.example.student.doneate.restaurant.FoodRequests;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ReqToConfirmAdapter extends BaseAdapter implements CompoundButton.OnCheckedChangeListener {
    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private ArrayList<String> checkedList = new ArrayList<String>();
    private static LayoutInflater inflater = null;

    private static String url_updt_req = "http://10.112.68.50/doneate/update_food_req.php";
    private static String serverUpdtReq = "https://doneate.000webhostapp.com/update_food_req.php";
    JSONParser jParser = new JSONParser();
    Date uploadDateTime,prepDateTime, reqDateTime;

    public ReqToConfirmAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        resetCheckedList();
    }
    public ArrayList<String> getCheckedList(){
        return checkedList;
    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        HashMap<String, String> requests = new HashMap<String, String>();

        requests = data.get(position);
        return requests.get(FragmentHomeQc.KEY_ID);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        String prepTime = "", uploadTime = "", reqTime = "";
        if(convertView == null)
            vi = inflater.inflate(R.layout.list_row_qc_to_confirm, null);
        TextView tvReqId = vi.findViewById(R.id.req_id);
        TextView tvItemName = vi.findViewById(R.id.tvitem_name);
        TextView tvReqQty = vi.findViewById(R.id.tvreq_qty);
        TextView tvUpload = vi.findViewById(R.id.tvupload);
        TextView tvPrep = vi.findViewById(R.id.tvprep);
        TextView tvReqTime = vi.findViewById(R.id.tvReqTime);
        TextView tvResName = vi.findViewById(R.id.resName);
//        TextView tvResAdd = vi.findViewById(R.id.resAdd);
        ImageView type = vi.findViewById(R.id.foodType);
        CheckBox checkBox = vi.findViewById(R.id.chkThisRequest);
        checkBox.setOnCheckedChangeListener(this);
        HashMap<String, String> food = new HashMap<String, String>();

        food = data.get(position);
        checkBox.setTag(position + "," + food.get(FragmentHomeQc.KEY_ID));
        checkBox.setChecked(checkedList.get(position) != "0");
        try {
            SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            uploadTime = food.get(FragmentHomeQc.KEY_UPLOAD_TIME);
            prepTime = food.get(FragmentHomeQc.KEY_PREP_TIME);
            reqTime = food.get(FragmentHomeQc.KEY_REQ_TIME);
            uploadDateTime = dateParser.parse(uploadTime);
            prepDateTime = dateParser.parse(prepTime);
            reqDateTime = dateParser.parse(reqTime);
            uploadTime = dateFormatter.format(uploadDateTime).toString();
            prepTime = dateFormatter.format(prepDateTime).toString();
            reqTime = dateFormatter.format(reqDateTime);
//            upload_time.setText(dateFormatter.format(uploadDateTime));
//            prep_time.setText(dateFormatter.format(prepDateTime));
//            System.out.println(dateFormatter.format(date));
//            uploadTime = sdf2.format(sdf.parse());
//            prepTime = sdf2.format(sdf.parse(food.get(AvailableFood.KEY_PREP_TIME)));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        tvReqId.setText(food.get(FragmentHomeQc.KEY_ID));
        tvItemName.setText(food.get(FragmentHomeQc.KEY_NAME));
        tvReqQty.setText(food.get(FragmentHomeQc.KEY_REQ_QTY) + " kg");
        tvUpload.setText(uploadTime);
        tvPrep.setText(prepTime);
        tvResName.setText(food.get(FragmentHomeQc.KEY_RES_NAME));
        tvReqTime.setText(reqTime);
//        tvResAdd.setText(food.get(FragmentHomeQc.KEY_RES_ADD));
        if(food.get(FragmentHomeQc.KEY_TYPE).equals("Veg"))
            type.setImageResource(R.drawable.veg);
        else if(food.get(FragmentHomeQc.KEY_TYPE).equals("Non-Veg"))
            type.setImageResource(R.drawable.non_veg);
        return vi;
    }

    public void resetCheckedList(){
        checkedList.clear();
        for(int i = 0; i < data.size(); i++ ){
            checkedList.add(i,"0");
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

        String arrayTag[] = ((String) compoundButton.getTag()).split(",");
        String key = arrayTag[1];
        int pos = Integer.parseInt(arrayTag[0]);
//        compoundButton.setChecked(checkedList.get(pos)!="0");
        if(b) {
            checkedList.set(pos, key);
//            Toast.makeText(activity,"Checked " + key + " " + pos,Toast.LENGTH_SHORT).show();
        }else{
            checkedList.set(pos,"0");
//            Toast.makeText(activity,"Unchecked " + key + " " + pos, Toast.LENGTH_SHORT).show();
        }
    }

    public void removeItem(int o){
        if(o < data.size())
            data.remove(o);
        ReqToConfirmAdapter.this.notifyDataSetChanged();
    }
    public void updateSingleRequest(String reqId, int position, Context context){
        new UpdateRequest(reqId, context).execute();
        data.remove(position);
        ReqToConfirmAdapter.this.notifyDataSetChanged();

    }

    public void clearAllData(){
        data.clear();
        ReqToConfirmAdapter.this.notifyDataSetChanged();
    }

    private class UpdateRequest extends AsyncTask<String, String, String> {
        String reqId;
        Context context;
        public UpdateRequest(String reqId, Context context){
            this.reqId = reqId;
            this.context = context;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String msg = "";
            List<NameValuePair> pairs = new ArrayList<>();
            pairs.add(new BasicNameValuePair("req_id", reqId));
            pairs.add(new BasicNameValuePair("qc",new SessionManager(context).getUserId()));
            JSONObject jsonObject = jParser.makeHttpRequest(url_updt_req,"POST",pairs);
            Log.d("Request Status:",jsonObject.toString());

            try{
                int success = jsonObject.getInt("success");
                if(success == 1){
                    msg = "Request Confirmed!";
                }else{
                    msg = "Error processing request";
                }
            }catch (JSONException j){
                j.printStackTrace();
                msg = "Could not update request";
            }catch (Exception e){
                msg = "Poor internet connection";
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Toast.makeText(activity, s, Toast.LENGTH_LONG).show();
        }
    }
}
