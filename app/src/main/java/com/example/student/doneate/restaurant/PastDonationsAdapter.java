package com.example.student.doneate.restaurant;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.student.doneate.R;
import com.example.student.doneate.ngo.CurrentRequests;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

public class PastDonationsAdapter extends RecyclerView.Adapter<PastDonationsAdapter.MyViewHolder>
   {

    private Context mContext;
    private List<PastDonations> mData;
    private Bitmap receivedBitmap,imgBitmap;
    String photo_url, ngoID;

    public PastDonationsAdapter(Context mContext, List<PastDonations> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardview_item_past_donations, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tvQty.setText(String.valueOf(mData.get(position).getCheckOutQty()) + " kg");
        holder.tvNgoName.setText(mData.get(position).getNgoName());
        holder.tvItemName.setText(mData.get(position).getItemName());
        holder.tvCheckOut.setText(mData.get(position).getCheckOutTime());
        holder.tvReqId.setText(mData.get(position).getDonId());
        holder.tvQcName.setText(mData.get(position).getQcName());
        ngoID = mData.get(position).getImgNgo();
        ngoID = ngoID.substring(ngoID.indexOf("/"));
        new GetImage(ngoID, holder.imgNgo).execute();
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tvReqId, tvQty, tvNgoName, tvItemName, tvCheckOut, tvQcName;
        ImageView imgNgo;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvReqId = itemView.findViewById(R.id.tvReqId);
            tvNgoName = itemView.findViewById(R.id.tvNgoName);
            tvItemName = itemView.findViewById(R.id.itemName);
            tvCheckOut = itemView.findViewById(R.id.tvCheckOut);
            tvQty = itemView.findViewById(R.id.tvReqQty);
            tvQcName = itemView.findViewById(R.id.tvQcCnf);
            imgNgo = itemView.findViewById(R.id.ngoImg);

        }
    }

    private class GetImage extends AsyncTask<Void, Void, Bitmap> {
        String name;
        ImageView imageView;
        public GetImage(String name, ImageView imageView){
            this.name = name;
            this.imageView = imageView;
        }
        @Override
        protected Bitmap doInBackground(Void... voids) {
            String serverGetPhoto = "https://doneate.000webhostapp.com/ngo_images" + name;
            String getResPhoto = "http://10.112.68.50/doneate/ngo_images" + name;
            try{
                URLConnection connection = new URL(getResPhoto).openConnection();
                connection.setConnectTimeout(1000 * 30);
                connection.setReadTimeout(1000 * 30);
                receivedBitmap = BitmapFactory.decodeStream((InputStream)connection.getContent(),null,null);
                return receivedBitmap;
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Bitmap bitmap){
            super.onPostExecute(bitmap);
            if(bitmap != null){
                imageView.setImageBitmap(bitmap);
//                imgBitmap = bitmap;
//                receivedBitmap = bitmap;

            }
        }
    }

}
